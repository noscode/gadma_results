# Result demographic models for 3 populations: YRI, CEU, CHB

## Data

Data is in `data` directory. AFS is the same that was used in [1].

## Models

Here are represented result GADMA models for YRI, CEU, CHB case.

* Model 1 - the same model as in Gutenkunst et al. [1].
* Model 2 - the same model as in Gutenkunst et al. with extra limitation on out of Africa time - 150 kya ago.
* Model 3 - model with structure (2,1,1) with all available parameters, with extra limitation on out of Africa time.

Folders of models contain dadi and moments code and can be run as follows:

```console
$ python model_2_dadi_code.py
```

Code that uses moments also draw model during run.

## Result of 100 runs of BFGS

One can find result of 100 runs of dadi's BFGS method (`dadi.Inference.optimize_log`) for the model from Gutenkunst et al. [1] in `results_of_100_runs_of_BFGS.txt`.
Best model has log likelihood equal to -6323.99. Model is not drawn.

## Pictures
### Model 1
![`model_1`](pictures/model_1.png)

### Model 2
![`model_2`](pictures/model_2.png)

### Model 3
![`model_3`](pictures/model_3.png)

One can find pictures in [`pictures`](https://bitbucket.org/noscode/gadma_results/src/master/2d_yri_ceu_no_lim/pictures) folder.

## References

[1] Gutenkunst, R. N., Hernandez, R. D., Williamson, S. H., and Bustamante, C. D. 2009. Inferring the joint demographic history of multiple populations from multidimensional snp frequency data. PLoS genetics , 5(10): e1000695.
