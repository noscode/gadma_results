#current best params = [7534.209999999999, 10528.049999999997, 0.1763526958933516, 22651.539999999997, 905.6599999999999, 0.8991453746439062, 6630.0599999999995, 5202.42, 10942.74, 1901.8199999999997, 865.4499999999999, 485.60999999999996, 1.9300000000000006e-06, 0.0013700000000000001, 0.000187, 9.720000000000002e-05, 2.1100000000000005e-05, 0.00020700000000000002, 6.320000000000002e-05, 0.000184]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np
def generated_model((nu11, s1, nu21, nu22, s2, nu31, nu32, nu33,
            t1, t2, t3, m1_12, m1_21, m2_12, m2_13, m2_21, m2_23, m2_31, m2_32), ns):
        theta1 = 0.37976
        sts = moments.LinearSystem_1D.steady_state_1D(sum(ns), theta=theta1)
        fs = moments.Spectrum(sts)

        before = [1.0]
        T = t1
        after = [nu11]
        growth_funcs = [lambda t: after[0]]
        list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
        fs.integrate(Npop=list_growth_funcs, tf=T, dt_fac=0.1, theta=theta1)

        before = after
        fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

        before.append((1 - s1) * before[-1])
        before[-2] *= s1
        T = t2
        after = [nu21, nu22]
        growth_funcs = [lambda t: after[0], lambda t: before[1] * (after[1] / before[1]) ** (t / T)]
        list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
        m = np.array([[0, m1_12],[m1_21, 0]])
        fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

        before = after
        fs = moments.Manips.split_2D_to_3D_2(fs, ns[1], ns[2])

        before.append((1 - s2) * before[-1])
        before[-2] *= s2
        T = t3
        after = [nu31, nu32, nu33]
        growth_funcs = [lambda t:  before[0] + (after[0] - before[0]) * (t / T), lambda t: before[1] * (after[1] / before[1]) ** (t / T), lambda t: before[2] * (after[2] / before[2]) ** (t / T)]
        list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
        m = np.array([[0.0, m2_12, m2_13],[m2_21, 0.0, m2_23], [m2_31, m2_32, 0.0]])
        fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

        before = after
        return fs
    
data = moments.Spectrum.from_file('../data/YRI.CEU.CHB.fs')

popt = [1.3575401788818946, 0.97124971295844331, 1.9262942701780066, 0.19902696939744066, 0.61309186121527925, 1.4961080637194906, 2.6867340197456206, 5.7811690016940656, 0.34208381080170686, 0.34891360545139094, 0.061550557890403097, 2.4825932124194527, 8.3869145148651381, 0.65001397134176675, 0.13392149863713737, 0.41428640947173534, 2.7558379776255504, 0.30261880843554062, 0.99878950654687981]
ns = [20, 20, 20]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

print('Drawing model to model_3.png')
model = moments.ModelPlot.generate_model(generated_model, popt, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_3.png',
	fig_title='',
	pop_labels=['YRI', 'CEU', 'CHB'],
	nref=6916,
	draw_scale=True,
	gen_time=0.025,
	gen_time_units="Thousand years",
	reverse_timeline=True)

try:
    import gadma
    print 'Calculation of CLAIC (time consuming, enter Ctrl+C to stop)'
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    try:
        for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
            try:
                claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=eps)
            except Exception, e:
                print('Error for eps = {0:.1e} : '.format(eps) + str(e))
                claic_score = None
            print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))
    except KeyboardInterrupt:
        import os
        os._exit(1)
except ImportError:
    print('Install GADMA to calculate CLAIC score')

