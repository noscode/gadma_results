#current best params = [7301.9990776049635, 9912.7571340072718, 0.97124971295844331, 14065.798984035531, 1453.2947469586231, 0.61309186121527925, 10924.579701277069, 19618.5293339524, 42214.090717848478, 2497.8956709376544, 2547.7668251698788, 449.44211694179432, 0.00033998815749422648, 0.0011485778655584305, 8.9018632354438703e-05, 1.8340388325694402e-05, 5.6736025993531099e-05, 0.00037740869977341302, 4.1443282205233963e-05, 0.00013678302283139703]
import dadi
def generated_model((nu11, s1, nu21, nu22, s2, nu31, nu32, nu33,
            t1, t2, t3, m1_12, m1_21, m2_12, m2_13, m2_21, m2_23, m2_31, m2_32), ns, pts):
        theta1 = 0.37976
        xx = dadi.Numerics.default_grid(pts)
        phi = dadi.PhiManip.phi_1D(xx, theta0=theta1, nu=1.0)
        before = [1.0]
        T = t1
        after = [nu11]
        growth_func = after[0]
        phi = dadi.Integration.one_pop(phi, xx, nu=growth_func, T=T, theta0=theta1)
        before = after
        phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
        before.append((1 - s1) * before[-1])
        before[-2] *= s1
        T = t2
        after = [nu21, nu22]
        growth_func_1 = after[0]
        growth_func_2 = lambda t: before[1] * (after[1] / before[1]) ** (t / T)
        phi = dadi.Integration.two_pops(phi, xx,  T=T, nu1=growth_func_1, nu2=growth_func_2, m12=m1_12, m21=m1_21, theta0=theta1)
        before = after
        phi = dadi.PhiManip.phi_2D_to_3D_split_2(xx, phi)
        before.append((1 - s2) * before[-1])
        before[-2] *= s2
        T = t3
        after = [nu31, nu32, nu33]
        growth_func_1 = lambda t: before[0] + (after[0] - before[0]) * (t / T)
        growth_func_2 = lambda t: before[1] * (after[1] / before[1]) ** (t / T)
        growth_func_3 = lambda t: before[2] * (after[2] / before[2]) ** (t / T)
        phi = dadi.Integration.three_pops(phi, xx,  T=T, nu1=growth_func_1, nu2=growth_func_2, nu3=growth_func_3, m12=m2_12, m13=m2_13, m21=m2_21, m23=m2_23, m31=m2_31, m32=m2_32, theta0=theta1)
        before = after
        sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*3)
        return sfs
data = dadi.Spectrum.from_file('../data/YRI.CEU.CHB.fs')

popt = [1.3575401788818946, 0.97124971295844331, 1.9262942701780066, 0.19902696939744066, 0.61309186121527925, 1.4961080637194906, 2.6867340197456206, 5.7811690016940656, 0.34208381080170686, 0.34891360545139094, 0.061550557890403097, 2.4825932124194527, 8.3869145148651381, 0.65001397134176675, 0.13392149863713737, 0.41428640947173534, 2.7558379776255504, 0.30261880843554062, 0.99878950654687981]
pts = [40, 50, 60]
ns = [20, 20, 20]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

try:
    import gadma
    print "Calculation of CLAIC (time consuming, enter Ctrl+C to stop)"
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    try:
        for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
            try:
                claic_score = gadma.Inference.get_claic_score(func_ex, all_boot, popt, data, pts=pts, eps=eps)
            except RuntimeError, e:
                print('Error for eps = {0:.1e} : '.format(eps) + str(e))
                claic_score = None
            print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))
    except KeyboardInterrupt:
        import os
        os._exit(1)
except ImportError:
    print('Install GADMA to calculate CLAIC score')
