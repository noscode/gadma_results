import dadi
def generated_model(params, ns, pts):
    nuAf, nuB, nuEu0, nuEu, nuAs0, nuAs, mAfB, mAfEu, mAfAs, mEuAs, TAf, TB, TEuAs = params
    n1, n2, n3 = ns
    theta = 0.37976
    xx = dadi.Numerics.default_grid(pts)
    phi = dadi.PhiManip.phi_1D(xx, theta0=theta)
    phi = dadi.Integration.one_pop(phi, xx, TAf, nu=nuAf, theta0=theta)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, TB, nu1=nuAf, nu2=nuB, m12=mAfB, m21=mAfB, theta0=theta)
    phi = dadi.PhiManip.phi_2D_to_3D_split_2(xx, phi)
    nuEu_func = lambda t: nuEu0*(nuEu/nuEu0)**(t/TEuAs)
    nuAs_func = lambda t: nuAs0*(nuAs/nuAs0)**(t/TEuAs)
    phi = dadi.Integration.three_pops(phi, xx, TEuAs, nu1=nuAf, nu2=nuEu_func , nu3=nuAs_func, m12=mAfEu , m13=mAfAs, m21=mAfEu, m23=mEuAs, m31=mAfAs, m32=mEuAs, theta0=theta)
    fs = dadi.Spectrum.from_phi(phi, (n1,n2,n3), (xx, xx, xx))
    return fs
data = dadi.Spectrum.from_file('../data/YRI.CEU.CHB.fs')

popt =  [1.68, 0.287, 0.129, 3.74, 0.070, 7.29, 3.65, 0.44, 0.28, 1.40, 0.607 - 0.396, 0.396 - 0.058, 0.058]
pts = [40, 50, 60]
ns = [20, 20, 20]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

claic_result = ''

try:
    import gadma
    print "Calculation of CLAIC (time consuming, enter Ctrl+C to stop)"
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    try:
        for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
            try:
                claic_score = gadma.Inference.get_claic_score(func_ex, all_boot, popt, data, pts=pts, eps=eps)
            except RuntimeError, e:
                print('Error for eps = {0:.1e} : '.format(eps) + str(e))
                claic_score = None
            print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))
    except KeyboardInterrupt:
            print("Already calculated result is:")
            print(claic_result)
            os._exit(0)
except ImportError:
    print('Install GADMA to calculate CLAIC score')
