#current best params = [5824.145260643522, 11840.480287927, 2053.7018644606305, 924.5817816067502, 24750.70572048937, 494.27298186883445, 49888.002972008835, 0.0005244033658506549, 6.129990598951586e-05, 3.832032930922414e-05, 0.00020357589166419715, 3487.2790618613235, 7577.421443640856, 419.27695174054156]
import dadi
def generated_model(params, ns, pts):
    nuAf, nuB, nuEu0, nuEu, nuAs0, nuAs, mAfB, mAfEu, mAfAs, mEuAs, TAf, TB, TEuAs = params
    n1, n2, n3 = ns
    theta = 0.37976
    xx = dadi.Numerics.default_grid(pts)
    phi = dadi.PhiManip.phi_1D(xx, theta0=theta)
    phi = dadi.Integration.one_pop(phi, xx, TAf, nu=nuAf, theta0=theta)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, TB, nu1=nuAf, nu2=nuB, m12=mAfB, m21=mAfB, theta0=theta)
    phi = dadi.PhiManip.phi_2D_to_3D_split_2(xx, phi)
    nuEu_func = lambda t: nuEu0*(nuEu/nuEu0)**(t/TEuAs)
    nuAs_func = lambda t: nuAs0*(nuAs/nuAs0)**(t/TEuAs)
    phi = dadi.Integration.three_pops(phi, xx, TEuAs, nu1=nuAf, nu2=nuEu_func , nu3=nuAs_func, m12=mAfEu , m13=mAfAs, m21=mAfEu, m23=mEuAs, m31=mAfAs, m32=mEuAs, theta0=theta)
    fs = dadi.Spectrum.from_phi(phi, (n1,n2,n3), (xx, xx, xx))
    return fs
data = dadi.Spectrum.from_file('../data/YRI.CEU.CHB.fs')

popt = [2.0329987934776748, 0.35261858565555637, 0.1587497804793061, 4.249671773769361, 0.08486618374868987, 8.565720932327261, 3.0542013778846027, 0.3570195569467322, 0.22318316433261684, 1.185655564617313, 0.5987623772755981, 1.3010357922981493, 0.07198943930430313]
pts = [40, 50, 60]
ns = [20, 20, 20]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

claic_result = '''Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-02)): -20254.0941117
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-03)): 63574.2270737
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-04)): 45182.7983259
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-05)): 40079.4752687
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-06)): 28323.6916629
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-07)): 12672.365773
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-08)): 12629.1474226'''

try:
    import gadma
    print "Calculation of CLAIC (time consuming, enter Ctrl+C to stop)"
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    try:
        for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
            try:
                claic_score = gadma.Inference.get_claic_score(func_ex, all_boot, popt, data, pts=pts, eps=eps)
            except RuntimeError, e:
                print('Error for eps = {0:.1e} : '.format(eps) + str(e))
                claic_score = None
            print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))
    except KeyboardInterrupt:
            print("Already calculated result is:")
            print(claic_score)
            os._exit()
except ImportError:
    print('Install GADMA to calculate CLAIC score')
