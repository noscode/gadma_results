#current best params = [5824.145260643522, 11840.480287927, 2053.7018644606305, 924.5817816067502, 24750.70572048937, 494.27298186883445, 49888.002972008835, 0.0005244033658506549, 6.129990598951586e-05, 3.832032930922414e-05, 0.00020357589166419715, 3487.2790618613235, 7577.421443640856, 419.27695174054156]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy
import os

def generated_model(params, ns):
    nuAf, nuB, nuEu0, nuEu, nuAs0, nuAs, mAfB, mAfEu, mAfAs, mEuAs, TAf, TB, TEuAs = params
    n1, n2, n3 = ns
    theta = 0.37976
    sts = moments.LinearSystem_1D.steady_state_1D(n1+n2+n3, theta=theta)
    fs = moments.Spectrum(sts)

    fs.integrate([nuAf], TAf, 0.05, theta=theta)
    
    fs = moments.Manips.split_1D_to_2D(fs, n1, n2+n3)
    
    mig1=numpy.array([[0, mAfB],[mAfB, 0]])
    fs.integrate([nuAf, nuB], TB, 0.05, m=mig1, theta=theta)
    
    fs = moments.Manips.split_2D_to_3D_2(fs, n2, n3)

    nuEu_func = lambda t: nuEu0*(nuEu/nuEu0)**(t/TEuAs)
    nuAs_func = lambda t: nuAs0*(nuAs/nuAs0)**(t/TEuAs)
    nu2 = lambda t: [nuAf, nuEu_func(t), nuAs_func(t)]
    mig2=numpy.array([[0, mAfEu, mAfAs],[mAfEu, 0, mEuAs],[mAfAs, mEuAs, 0]])
    
    fs.integrate(nu2, TEuAs, 0.05, m=mig2, theta=theta)
                                
    return fs
data = moments.Spectrum.from_file('../data/YRI.CEU.CHB.fs')

popt = [2.0329987934776748, 0.35261858565555637, 0.1587497804793061, 4.249671773769361, 0.08486618374868987, 8.565720932327261, 3.0542013778846027, 0.3570195569467322, 0.22318316433261684, 1.185655564617313, 0.5987623772755981, 1.3010357922981493, 0.07198943930430313]
ns = [20, 20, 20]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = moments.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

#now we need to norm vector of params so that first value is 1:
popt_norm = [2.0329987934776748, 0.35261858565555637, 0.1587497804793061, 4.249671773769361, 0.08486618374868987, 8.565720932327261, 3.0542013778846027, 0.3570195569467322, 0.22318316433261684, 1.185655564617313, 0.5987623772755981, 1.3010357922981493, 0.07198943930430313]
print('Drawing model to model_as_in_Gutenkunst_et_al.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_as_in_Gutenkunst_et_al.png',
	fig_title='',
	pop_labels=['YRI', 'CEU', 'CHB'],
	nref=5824,
	draw_scale=True,
	gen_time=0.025,
	gen_time_units="Thousand years",
	reverse_timeline=True)

claic_result='''Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-02)): 35299.1976891
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-03)): 35480.6778164
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-04)): 35482.6303928
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-05)): 35496.7500619
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-06)): 39297.2630958
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-07)): 25061.1198643
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-08)): 15402.9471477'''

try:
    import gadma
    print 'Calculation of CLAIC (time consuming, enter Ctrl+C to stop)'
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    try:
        for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
            try:
                claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=eps)
            except RuntimeError, e:
                print('Error for eps = {0:.1e} : '.format(eps) + str(e))
                claic_score = None
            print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))
    except KeyboardInterrupt:
        print("Already calculated result is:")
        print(claic_score)
        os._exit()
except ImportError:
    print('Install GADMA to calculate CLAIC score')

