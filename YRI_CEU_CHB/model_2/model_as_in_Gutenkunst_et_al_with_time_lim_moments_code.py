#current best params = [7294.524355481658, 12207.258189175449, 2067.760158851058, 952.3066290007108, 23689.15411453231, 507.6606345084994, 46236.502916528385, 0.0005065411323340735, 6.287981425703642e-05, 4.019708054268921e-05, 0.00020312547594194516, 1641.758918973322, 2577.575396165549, 422.42460383445183]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy
def generated_model(params, ns):
    nuAf, nuB, nuEu0, nuEu, nuAs0, nuAs, mAfB, mAfEu, mAfAs, mEuAs, TAf, TB, TEuAs = params
    n1, n2, n3 = ns
    theta = 0.37976
    sts = moments.LinearSystem_1D.steady_state_1D(n1+n2+n3, theta=theta)
    fs = moments.Spectrum(sts)

    fs.integrate([nuAf], TAf, 0.05, theta=theta)
    
    fs = moments.Manips.split_1D_to_2D(fs, n1, n2+n3)
    
    mig1=numpy.array([[0, mAfB],[mAfB, 0]])
    fs.integrate([nuAf, nuB], TB, 0.05, m=mig1, theta=theta)
    
    fs = moments.Manips.split_2D_to_3D_2(fs, n2, n3)

    nuEu_func = lambda t: nuEu0*(nuEu/nuEu0)**(t/TEuAs)
    nuAs_func = lambda t: nuAs0*(nuAs/nuAs0)**(t/TEuAs)
    nu2 = lambda t: [nuAf, nuEu_func(t), nuAs_func(t)]
    mig2=numpy.array([[0, mAfEu, mAfAs],[mAfEu, 0, mEuAs],[mAfAs, mEuAs, 0]])
    
    fs.integrate(nu2, TEuAs, 0.05, m=mig2, theta=theta)
                                
    return fs
data = moments.Spectrum.from_file('../data/YRI.CEU.CHB.fs')

popt = [1.6734824087607565, 0.28346744188977674, 0.1305508875688482, 3.247525535606785, 0.0695947548830932, 6.33852197392182, 3.6949766268641566, 0.45867833656611495, 0.2932185830379043, 1.4817037314773225, 0.22506730239917289, 0.35335756939773105, 0.05790982156595447]
ns = [20, 20, 20]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [1.6734824087607565, 0.28346744188977674, 0.1305508875688482, 3.247525535606785, 0.0695947548830932, 6.33852197392182, 3.6949766268641566, 0.45867833656611495, 0.2932185830379043, 1.4817037314773225, 0.22506730239917289, 0.35335756939773105, 0.05790982156595447]
print('Drawing model to result_limited_model.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='result_limited_model.png',
	fig_title='',
	pop_labels=['YRI', 'CEU', 'CHB'],
	nref=7294,
	draw_scale=True,
	gen_time=0.025,
	gen_time_units="Thousand years",
	reverse_timeline=True)

claic_result = '''Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-02)): 210828.670308
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-03)): 210895.129308
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-04)): 210853.807486
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-05)): 203818.717386
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-06)): 54621.8135548
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-07)): 25621.2036142
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-08)): 12626.374996'''

try:
    import gadma
    print 'Calculation of CLAIC (time consuming, enter Ctrl+C to stop)'
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    try:
        for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
            try:
                claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=eps)
            except RuntimeError, e:
                print('Error for eps = {0:.1e} : '.format(eps) + str(e))
                claic_score = None
            print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))
    except KeyboardInterrupt:
        print("Already calculated result is:")
        print(claic_score)
        os._exit()
except ImportError:
    print('Install GADMA to calculate CLAIC score')

