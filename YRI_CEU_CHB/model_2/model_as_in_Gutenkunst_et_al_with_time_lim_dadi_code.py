#current best params = [7294.524355481658, 12207.258189175449, 2067.760158851058, 952.3066290007108, 23689.15411453231, 507.6606345084994, 46236.502916528385, 0.0005065411323340735, 6.287981425703642e-05, 4.019708054268921e-05, 0.00020312547594194516, 1641.758918973322, 2577.575396165549, 422.42460383445183]
import dadi
def generated_model(params, ns, pts):
    nuAf, nuB, nuEu0, nuEu, nuAs0, nuAs, mAfB, mAfEu, mAfAs, mEuAs, TAf, TB, TEuAs = params
    n1, n2, n3 = ns
    theta = 0.37976
    xx = dadi.Numerics.default_grid(pts)
    phi = dadi.PhiManip.phi_1D(xx, theta0=theta)
    phi = dadi.Integration.one_pop(phi, xx, TAf, nu=nuAf, theta0=theta)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, TB, nu1=nuAf, nu2=nuB, m12=mAfB, m21=mAfB, theta0=theta)
    phi = dadi.PhiManip.phi_2D_to_3D_split_2(xx, phi)
    nuEu_func = lambda t: nuEu0*(nuEu/nuEu0)**(t/TEuAs)
    nuAs_func = lambda t: nuAs0*(nuAs/nuAs0)**(t/TEuAs)
    phi = dadi.Integration.three_pops(phi, xx, TEuAs, nu1=nuAf, nu2=nuEu_func , nu3=nuAs_func, m12=mAfEu , m13=mAfAs, m21=mAfEu, m23=mEuAs, m31=mAfAs, m32=mEuAs, theta0=theta)
    fs = dadi.Spectrum.from_phi(phi, (n1,n2,n3), (xx, xx, xx))
    return fs
data = dadi.Spectrum.from_file('../data/YRI.CEU.CHB.fs')

popt = [1.6734824087607565, 0.28346744188977674, 0.1305508875688482, 3.247525535606785, 0.0695947548830932, 6.33852197392182, 3.6949766268641566, 0.45867833656611495, 0.2932185830379043, 1.4817037314773225, 0.22506730239917289, 0.35335756939773105, 0.05790982156595447]
pts = [40, 50, 60]
ns = [20, 20, 20]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

claic_result = '''Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-02)): 45528.8187715
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-03)): 9945.04444976
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-04)): 46161.5001972
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-05)): 55876.6207965
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-06)): 1851.53976342
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-07)): 12479.1226226
Model Composite Likelihood AIC score (CLAIC(p0, eps=1.0e-08)): 12621.0594478'''

try:
    import gadma
    print "Calculation of CLAIC (time consuming, enter Ctrl+C to stop)"
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    try:
        for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
            try:
                claic_score = gadma.Inference.get_claic_score(func_ex, all_boot, popt, data, pts=pts, eps=eps)
            except RuntimeError, e:
                print('Error for eps = {0:.1e} : '.format(eps) + str(e))
                claic_score = None
            print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))
    except KeyboardInterrupt:
        print("Already calculated result is:")
        print(claic_score)
        os._exit()

except ImportError:
    print('Install GADMA to calculate CLAIC score')
