import moments
import numpy as np
def no_mig_size(params, ns):
    """
    Split with no migration, then size change with no migration.
    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    T1: Time in the past of split (in units of 2*Na generations)
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    T2: Time of population size change.
    """
    nu1a, nu2a, nu1b, nu2b, T1, T2 = params

    sts = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sts)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    fs.integrate(Npop=[nu1a, nu2a], tf=T1, m=np.array([[0.0, 0.0],[0.0, 0.0]]))
    fs.integrate(Npop=[nu1b, nu2b], tf=T2, m=np.array([[0.0, 0.0],[0.0, 0.0]]))
    
    return fs

def sec_contact_sym_mig_size(params, ns):
    """
    Split with no gene flow, followed by size change with symmetrical gene flow.
    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    T2: The scale time between the secondary contact and present.
    m: Migration between pop 2 and pop 1.
    """
    nu1a, nu2a, nu1b, nu2b, m, T1, T2 = params

    sts = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sts)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    fs.integrate(Npop=[nu1a, nu2a], tf=T1, m=np.array([[0.0, 0.0],[0.0, 0.0]]))
    fs.integrate(Npop=[nu1b, nu2b], tf=T2, m=np.array([[0.0, m],[m, 0.0]]))
    
    return fs


def anc_sym_mig_size(params, ns):
    """
    Split with symmetrical gene flow, followed by size change with no gene flow.  
    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    T1: Time in the past of split (in units of 2*Na generations)
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    T2: The scale time between the ancient migration and present.
    m: Migration between pop 2 and pop 1.
    """
    nu1a, nu2a, nu1b, nu2b, m, T1, T2 = params

    sts = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sts)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    fs.integrate(Npop=[nu1a, nu2a], tf=T1, m=np.array([[0.0, m],[m, 0.0]]))
    fs.integrate(Npop=[nu1b, nu2b], tf=T2, m=np.array([[0.0, 0.0],[0.0, 0.0]]))
    
    return fs

def anc_asym_mig_size(params, ns):
    """
    Split with asymmetrical gene flow, followed by size change with no gene flow.
    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    T1: Time in the past of split (in units of 2*Na generations)
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    T2: The scale time between the ancient migration and present.
    m12: Migration from pop 2 to pop 1 (2*Na*m12).
    m21: Migration from pop 1 to pop 2.
    """
    nu1a, nu2a, nu1b, nu2b, m12, m21, T1, T2 = params
    
    sts = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sts)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    fs.integrate(Npop=[nu1a, nu2a], tf=T1, m=np.array([[0.0, m12],[m21, 0.0]]))
    fs.integrate(Npop=[nu1b, nu2b], tf=T2, m=np.array([[0.0, 0.0],[0.0, 0.0]]))
    
    return fs
