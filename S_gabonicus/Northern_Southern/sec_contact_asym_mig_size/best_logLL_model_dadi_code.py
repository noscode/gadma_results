#current best params = [1.1281895027052304, 1.2459422928235617, 6.97192993147781, 2.9060638845875393, 0.0190797104369048, 0.03323984436223855, 1.6212222495689974, 1.0905270815469033]
import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "Northern_Southern/sec_contact_asym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = dadi.Misc.make_data_dict('data/dadi_2pops_Northern_Southern_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['Northern', 'Southern'], projections=[40, 18], polarized=False)

popt = [1.1281895027052304, 1.2459422928235617, 6.97192993147781, 2.9060638845875393, 0.0190797104369048, 0.03323984436223855, 1.6212222495689974, 1.0905270815469033]
pts = [50, 60, 70]
ns = [40, 18]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))
