#current best params = [0.98673983858365233, 1.3042786468882819, 7.0454378828699866, 3.0433021456869422, 0.054950496005290221, 0.020964870525017273, 1.7206609213190394, 1.1808694858346653]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func= file_with_model_func", "Northern_Southern/unidir_m12_sym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = moments.Misc.make_data_dict('data/dadi_2pops_Northern_Southern_snps.txt')
data = moments.Spectrum.from_data_dict(dd, pop_ids=['Northern', 'Southern'], projections=[40, 18], polarized=False)

popt = [0.98673983858365233, 1.3042786468882819, 7.0454378828699866, 3.0433021456869422, 0.054950496005290221, 0.020964870525017273, 1.7206609213190394, 1.1808694858346653]
ns = [40, 18]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [0.98673983858365233, 1.3042786468882819, 7.0454378828699866, 3.0433021456869422, 0.054950496005290221, 0.020964870525017273, 1.7206609213190394, 1.1808694858346653]
print('Drawing model to model_from_GADMA.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['Northern', 'Southern'],
	nref=1,
	draw_scale=True,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
