#current best params = [2.515351553880708, 3.3388374466172808, 17.139095530675654, 7.057400983494733, 0.033308413598636774, 0.006168636809380847, 0.013628549353710767, 5.7791786748238385, 2.6916397213419296]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func= file_with_model_func", "Northern_Southern/unidir_m12_asym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = moments.Misc.make_data_dict('data/dadi_2pops_Northern_Southern_snps.txt')
data = moments.Spectrum.from_data_dict(dd, pop_ids=['Northern', 'Southern'], projections=[40, 18], polarized=False)

popt = [2.515351553880708, 3.3388374466172808, 17.139095530675654, 7.057400983494733, 0.033308413598636774, 0.006168636809380847, 0.013628549353710767, 5.7791786748238385, 2.6916397213419296]
ns = [40, 18]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [2.515351553880708, 3.3388374466172808, 17.139095530675654, 7.057400983494733, 0.033308413598636774, 0.006168636809380847, 0.013628549353710767, 5.7791786748238385, 2.6916397213419296]
print('Drawing model to model_from_GADMA.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['Northern', 'Southern'],
	nref=1,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
