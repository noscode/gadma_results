#current best params = [134.71167154132587, 0.39461279628343349, 255.18416304528031, 354.77203562773951, 1824.5365437636794, 752.25492852609045, 584.01579471594698, 291.31036050319148, 0.00033753512568671714, 0.0, 5.8423738388327837e-05, 0.00012371806455572373]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np
def generated_model(params, ns):
	Ns = params[:6]
	Ts = params[6:8]
	Ms = params[8:]
	theta1 = 1
	sts = moments.LinearSystem_1D.steady_state_1D(sum(ns), theta=theta1, N=Ns[0])
	fs = moments.Spectrum(sts)

	before = [Ns[0]]
	fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[0]
	after = Ns[2:4]
	growth_funcs = [lambda t: after[0], lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, params[8]],[params[9], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	T = Ts[1]
	after = Ns[4:6]
	growth_funcs = [lambda t: after[0], lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, params[10]],[params[11], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	return fs
dd = moments.Misc.make_data_dict('data/dadi_2pops_Northern_Southern_snps.txt')
data = moments.Spectrum.from_data_dict(dd, pop_ids=['Northern', 'Southern'], projections=[40, 18], polarized=False)

popt = [134.71167154132587, 0.39461279628343349, 255.18416304528031, 354.77203562773951, 1824.5365437636794, 752.25492852609045, 584.01579471594698, 291.31036050319148, 0.00033753512568671714, 0.0, 5.8423738388327837e-05, 0.00012371806455572373]
ns = [40, 18]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll(model, data)
ll_true = moments.Inference.ll(data, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [1.0, 0.39461279628343349, 1.8942988393325426, 2.6335656856496295, 13.544012355335976, 5.584185244819853, 4.335302116244522, 2.1624730594618553, 0.045469920985169186, 0.0, 0.0078703594559847712, 0.016666267276159205]
print('Drawing model to model_from_GADMA.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['Northern', 'Southern'],
	nref=134,
	draw_scale=True,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
