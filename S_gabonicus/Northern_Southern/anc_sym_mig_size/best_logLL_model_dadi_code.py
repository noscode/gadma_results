#current best params = [2.1449662366871887, 2.219529922651325, 11.999999999469019, 4.450020725680598, 0.06755483017044676, 3.8057411285309195, 1.05104717107318]
import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "Northern_Southern/anc_sym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = dadi.Misc.make_data_dict('data/dadi_2pops_Northern_Southern_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['Northern', 'Southern'], projections=[40, 18], polarized=False)

popt = [2.1449662366871887, 2.219529922651325, 11.999999999469019, 4.450020725680598, 0.06755483017044676, 3.8057411285309195, 1.05104717107318]
popt = [     5.182,      5.228,     27.808,     10.110,      0.028,     10.000,      2.218]
pts = [50, 60, 70]
ns = [40, 18]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))
