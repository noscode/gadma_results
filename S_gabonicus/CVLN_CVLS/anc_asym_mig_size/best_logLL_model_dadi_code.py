#current best params = [5.822415510287434, 1.3677560397748052, 11.999999999971255, 12.0, 0.09076999133735597, 0.7685300216299854, 1.6597494414577778, 0.09003206728005093]
import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "CVLN_CVLS/anc_asym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = dadi.Misc.make_data_dict('data/dadi_2pops_CVLN_CVLS_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['CVLN', 'CVLS'], projections=[30, 18], polarized=False)

popt = [5.822415510287434, 1.3677560397748052, 11.999999999971255, 12.0, 0.09076999133735597, 0.7685300216299854, 1.6597494414577778, 0.09003206728005093]
pts = [40, 50, 60]
ns = [30, 18]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))
