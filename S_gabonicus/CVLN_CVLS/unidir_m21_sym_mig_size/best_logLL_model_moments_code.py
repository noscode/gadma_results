#current best params = [6.809941323212305, 1.8845818407206751, 20.309207588958618, 5.786303353204261, 0.5922362205743824, 0.11225714509261436, 3.128753558801493, 0.3125576982726619]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func= file_with_model_func", "CVLN_CVLS/unidir_m21_sym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = moments.Misc.make_data_dict('data/dadi_2pops_CVLN_CVLS_snps.txt')
data = moments.Spectrum.from_data_dict(dd, pop_ids=['CVLN', 'CVLS'], projections=[30, 18], polarized=False)

popt = [6.809941323212305, 1.8845818407206751, 20.309207588958618, 5.786303353204261, 0.5922362205743824, 0.11225714509261436, 3.128753558801493, 0.3125576982726619]
ns = [30, 18]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [6.809941323212305, 1.8845818407206751, 20.309207588958618, 5.786303353204261, 0.5922362205743824, 0.11225714509261436, 3.128753558801493, 0.3125576982726619]
print('Drawing model to model_from_GADMA.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['CVLN', 'CVLS'],
	nref=1,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
