#current best params = [134.17362260356595, 0.32005933520153601, 268.78474332185351, 113.37279490162888, 1773.5215914673149, 485.7861642963133, 670.86811301769831, 264.90324483483136, 0.0, 0.0029792693677603132, 0.00031221066028218407, 0.0027173506642910812]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np
def generated_model(params, ns):
	Ns = params[:6]
	Ts = params[6:8]
	Ms = params[8:]
	theta1 = 1
	sts = moments.LinearSystem_1D.steady_state_1D(sum(ns), theta=theta1, N=Ns[0])
	fs = moments.Spectrum(sts)

	before = [Ns[0]]
	fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[0]
	after = Ns[2:4]
	growth_funcs = [lambda t: after[0], lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, params[8]],[params[9], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	T = Ts[1]
	after = Ns[4:6]
	growth_funcs = [lambda t: after[0], lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, params[10]],[params[11], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	return fs
dd = moments.Misc.make_data_dict('data/dadi_2pops_CVLN_CVLS_snps.txt')
data = moments.Spectrum.from_data_dict(dd, pop_ids=['CVLN', 'CVLS'], projections=[30, 18], polarized=False)

popt = [134.17362260356595, 0.32005933520153601, 268.78474332185351, 113.37279490162888, 1773.5215914673149, 485.7861642963133, 670.86811301769831, 264.90324483483136, 0.0, 0.0029792693677603132, 0.00031221066028218407, 0.0027173506642910812]
ns = [30, 18]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll(model, data)
ll_true = moments.Inference.ll(data, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [1.0, 0.32005933520153601, 2.0032606864615579, 0.84497081245696171, 13.218109171185038, 3.6205787312729418, 4.9999999999990203, 1.9743317627900956, 0.0, 0.39973936378423675, 0.041890435305511897, 0.36459678251214073]
print('Drawing model to model_from_GADMA.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['CVLN', 'CVLS'],
	nref=134,
	draw_scale=True,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
