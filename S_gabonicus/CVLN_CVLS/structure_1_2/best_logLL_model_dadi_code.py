#current best params = [134.17362260356595, 0.32005933520153601, 268.78474332185351, 113.37279490162888, 1773.5215914673149, 485.7861642963133, 670.86811301769831, 264.90324483483136, 0.0, 0.0029792693677603132, 0.00031221066028218407, 0.0027173506642910812]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:6]
	Ts = params[6:8]
	Ms = params[8:]
	theta1 = 1
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1, nu=Ns[0])
	before = [Ns[0]]
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[0]
	after = Ns[2:4]
	growth_func_1 = after[0]
	growth_func_2 = after[1]
	phi = dadi.Integration.two_pops(phi, xx,  T=T,nu1=growth_func_1, nu2=growth_func_2, m12=params[8], m21=params[9], theta0=theta1)
	before = after
	T = Ts[1]
	after = Ns[4:6]
	growth_func_1 = after[0]
	growth_func_2 = after[1]
	phi = dadi.Integration.two_pops(phi, xx,  T=T,nu1=growth_func_1, nu2=growth_func_2, m12=params[10], m21=params[11], theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs
dd = dadi.Misc.make_data_dict('data/dadi_2pops_CVLN_CVLS_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['CVLN', 'CVLS'], projections=[30, 18], polarized=False)

popt = [134.17362260356595, 0.32005933520153601, 268.78474332185351, 113.37279490162888, 1773.5215914673149, 485.7861642963133, 670.86811301769831, 264.90324483483136, 0.0, 0.0029792693677603132, 0.00031221066028218407, 0.0027173506642910812]
pts = [40, 50, 60]
ns = [30, 18]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll(model, data)
ll_true = dadi.Inference.ll(data, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
