#current best params = [1.89283589906787, 0.8801897927099807, 12.265977807722528, 3.349345554101201, 0.3987957046065245, 0.04607082863693359, 0.394934705197296, 4.147569340061689, 1.8083428976464326]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func= file_with_model_func", "CVLN_CVLS/unidir_m21_asym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = moments.Misc.make_data_dict('data/dadi_2pops_CVLN_CVLS_snps.txt')
data = moments.Spectrum.from_data_dict(dd, pop_ids=['CVLN', 'CVLS'], projections=[30, 18], polarized=False)

popt = [1.89283589906787, 0.8801897927099807, 12.265977807722528, 3.349345554101201, 0.3987957046065245, 0.04607082863693359, 0.394934705197296, 4.147569340061689, 1.8083428976464326]
ns = [30, 18]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [1.89283589906787, 0.8801897927099807, 12.265977807722528, 3.349345554101201, 0.3987957046065245, 0.04607082863693359, 0.394934705197296, 4.147569340061689, 1.8083428976464326]
print('Drawing model to model_from_GADMA.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['CVLN', 'CVLS'],
	nref=1,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
