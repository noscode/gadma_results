#current best params = [6.3332516540636945, 1.7576065089373418, 0.08540469759348654, 0.5571581117503274, 1.5740186056464849, 3.452372512870877e-05]
import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "CVLN_CVLS/anc_asym_mig/dem_model.py")
generated_model = file_with_model_func.model_func
dd = dadi.Misc.make_data_dict('data/dadi_2pops_CVLN_CVLS_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['CVLN', 'CVLS'], projections=[30, 18], polarized=False)

popt = [6.3332516540636945, 1.7576065089373418, 0.08540469759348654, 0.5571581117503274, 1.5740186056464849, 3.452372512870877e-05]
pts = [40, 50, 60]
ns = [30, 18]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))
