# Gaboon forest frog *Scotobleps gabonicus*

Here lies all scripts that were used to perfom demographic model selection for Gaboon forest frog *Scotobleps gabonicus*.

## Data

Directory `dada` contains AFS data from [1]. There are three spectra: each for one set of two populations:

* `data/dadi_2pops_Northern_Southern_snps.txt` - Northern, Southern 
* `data/dadi_2pops_CVLN_CVLS_snps.txt` - CVLN, CVLS
* `data/dadi_2pops_CrossRiver_CVLN_snps.txt` - CrossRiver, CVLN

## Results

All result have already been put here in corresponding directories. Each directory named after populations that were observed (`Northern_Southern`, `CVLN_CVLS`, `CrossRiver_CVLN`) contain folders named by demographic models (`no_mig`, `sec_contact_asym_mig_size`, `structure_1_2` and so on) with the following files:

* best_logLL_model_dadi_code.py - result of 10 GADMA runs.

* best_logLL_model.png - SFS or demographic model and SFS plots of final model.

* GADMA.log - output of GADMA.

* params - parameters that were used for GADMA run.

To get log-likelihood value of result model:
```console
$ python CrossRiver_CVLN/anc_asym_mig/best_logLL_model_dadi_code.py
Model log likelihood (LL(model, data)): -377.834666596
Optimal value of theta: 259.108455182
```

One can run GADMA for exactly data and model the following way:
```console
$ gadma -p params CVLN_CVLS/asym_mig_size/params
```

However the best way to perform model selection is using of scripts that is described below.

## Model selection of models from *dadi pipeline*

In Portik et al., 2017 [1] model selection was performed with [*dadi pipeline*](https://github.com/dportik/dadi_pipeline). There also exist file with implemented demographic models. We copied it here in file `models_from_dadi_pipeline.py`. Also we provide file `models_from_dadi_pipeline_moments_code.py` with the some models implemented with *moments*.


Overall to run the same model selection as it was done in Portik et al., 2017 [1] one should run script `optimize_models_from_dadi_pipeline.py` with corresponding AFS data file, for example:

```console
$ python optimize_models_from_dadi_pipeline.py data/dadi_2pops_Northern_Southern_snps.txt
```

## Models with (1,2) structure

We provide script that infer demographic model with structure equal to (1,2) for each of three datasets:

```console
$ python optimize_models_with_1_2_structure.py data/dadi_2pops_CVLN_CVLS_snps.txt
```

## Models with unidirectional migrations

At least we inferred demographic models with two time intervals after split% first with unidirectional migration and second with bidirectional migration and population size change. Models code is in `our_models_dadi_code.py` and `our_models_moments_code.py` files. To repeat runs:

```console
$ python optimize_our_models.py data/dadi_2pops_CrossRiver_CVLN_snps.txt
```

## References

[1] Portik,D.M.,Leach ́e,A.D.,Rivera,D.,Barej,M.F., Burger,M.,Hirschfeld,M.,Ro ̈del,M.-O.,Blackburn, D. C., and Fujita, M. K. 2017. Evaluating mechanisms of diversification in a guineo-congolian tropical forest frog using demographic model selection. Molecular ecology, 26(19): 5245–5263.

