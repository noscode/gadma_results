import sys
import numpy as np
import moments

def unidir_m12_sym_mig_size(params, ns):
    """
    Split with unidirectional gene flow, followed by size change with symmetrical gene flow.

    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    m12a: Unidirectional migration from pop 2 to pop 1.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    mb: Migration between pop 1 and pop 2.
    T2: The scale time between the secondary contact and present.
    """
    nu1a, nu2a, nu1b, nu2b, m12a, mb, T1, T2 = params

    sts = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sts)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    fs.integrate(Npop=[nu1a, nu2a], tf=T1, m=np.array([[0.0, m12a],[0.0, 0.0]]))
    fs.integrate(Npop=[nu1b, nu2b], tf=T2, m=np.array([[0.0, mb],[mb, 0.0]]))

    return fs

def unidir_m21_sym_mig_size(params, ns):
    """
    Split with unidirectional gene flow, followed by size change with symmetrical gene flow.

    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    m21a: Unidirectional migration from pop 1 to pop 2.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    mb: Migration between pop 1 and pop 2.
    T2: The scale time between the secondary contact and present.
    """
    nu1a, nu2a, nu1b, nu2b, m21a, mb, T1, T2 = params

    sts = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sts)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    fs.integrate(Npop=[nu1a, nu2a], tf=T1, m=np.array([[0.0, 0.0],[m21a, 0.0]]))
    fs.integrate(Npop=[nu1b, nu2b], tf=T2, m=np.array([[0.0, mb],[mb, 0.0]]))

    return fs

def unidir_m12_asym_mig_size(params, ns):
    """
    Split with unidirectional gene flow, followed by size change with asymmetrical gene flow.

    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    m12a: Unidirectional migration from pop 2 to pop 1.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    m12b: Migration from pop 2 to pop 1.
    m21b: Migration from pop 2 to pop 1.
    T2: The scale time between the secondary contact and present.
    """
    nu1a, nu2a, nu1b, nu2b, m12a, m12b, m21b, T1, T2 = params

    sts = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sts)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    fs.integrate(Npop=[nu1a, nu2a], tf=T1, m=np.array([[0.0, m12a],[0.0, 0.0]]))
    fs.integrate(Npop=[nu1b, nu2b], tf=T2, m=np.array([[0.0, m12b],[m21b, 0.0]]))

    return fs

def unidir_m21_asym_mig_size(params, ns):
    """
    Split with unidirectional gene flow, followed by size change with asymmetrical gene flow.

    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    m21a: Unidirectional migration from pop 1 to pop 2.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    m12b: Migration from pop 2 to pop 1.
    m21b: Migration from pop 2 to pop 1.
    T2: The scale time between the secondary contact and present.
    """
    nu1a, nu2a, nu1b, nu2b, m21a, m12b, m21b, T1, T2 = params

    sts = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sts)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    fs.integrate(Npop=[nu1a, nu2a], tf=T1, m=np.array([[0.0, 0.0],[m21a, 0.0]]))
    fs.integrate(Npop=[nu1b, nu2b], tf=T2, m=np.array([[0.0, m12b],[m21b, 0.0]]))

    return fs
