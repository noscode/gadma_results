import sys
import os 

if len(sys.argv) == 1:
    print "Please enter SFS file. For example:\n" + sys.argv[0] + 'data/dadi_2pops_Northern_Southern_snps.txt'
    os._exit(1)

spectrum_file = sys.argv[1]
spectrum_file = os.path.abspath(spectrum_file)

# Information about data: name, pop labels and dadi pts
tested_pops = { 'Northern_Southern': [['Northern', 'Southern'], ['40', '18'], ['50', '60', '70']],
                'CVLN_CVLS': [['CVLN', 'CVLS'], ['30', '18'], ['40', '50', '60']],
                'CrossRiver_CVLN': [['CrossRiver', 'CVLN'], ['14', '30'], ['40', '50', '60']]}

dirname = spectrum_file.split('/')[-1][len('dadi_2pops_'):-(len('_snps.txt'))]

if dirname not in tested_pops:
    print("File " + sys.argv[1] + " is not valid.")
    os._exit(1)
    
if not os.path.exists(dirname):
    os.makedirs(dirname)

model_name = 'structure_1_2'

print model_name
model_dirname = os.path.join(dirname, model_name)
if not os.path.exists(model_dirname):
    os.makedirs(model_dirname)

# Create file with parameters for GADMA
with open(os.path.join(model_dirname, 'params'), 'w') as f:
    f.write('Output directory: ' + os.path.join(model_dirname, 'gadma_outdir') + '\n')
    f.write('Input file: ' + spectrum_file + '\n')
    f.write('Population labels: ' + ', '.join(tested_pops[dirname][0]) + '\n')
    f.write('Projections: ' + ', '.join(tested_pops[dirname][1]) + '\n')
    f.write('Initial structure: 1,2\n')
    f.write('Use moments or dadi: moments\n')
    f.write('Pts : ' + ', '.join(tested_pops[dirname][2]) + '\n')
    f.write('Number of repeats: 10\nNumber of processes: 2\n')
    f.write('Silence: True\n')

# Run GADMA
from subprocess import call
call(["gadma", "-p", os.path.join(model_dirname, 'params')])
