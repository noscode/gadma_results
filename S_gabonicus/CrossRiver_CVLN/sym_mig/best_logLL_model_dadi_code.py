#current best params = [0.6384648321875309, 6.672386748454203, 0.35471871284252304, 1.0706395259810944]
import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "CrossRiver_CVLN/sym_mig/dem_model.py")
generated_model = file_with_model_func.model_func
dd = dadi.Misc.make_data_dict('data/dadi_2pops_CrossRiver_CVLN_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['CrossRiver', 'CVLN'], projections=[14, 30], polarized=False)

popt = [0.6384648321875309, 6.672386748454203, 0.35471871284252304, 1.0706395259810944]
pts = [40, 50, 60]
ns = [14, 30]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))
