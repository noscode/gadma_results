#current best params = [0.13884385306612057, 6.898571947781088, 0.8892858357817505, 8.873353884421421, 2.639000356549434, 0.5557370050463042, 0.3119384036173035, 1.0892571235911248, 0.10875987615580193]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func= file_with_model_func", "CrossRiver_CVLN/unidir_m12_asym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = moments.Misc.make_data_dict('data/dadi_2pops_CrossRiver_CVLN_snps.txt')
data = moments.Spectrum.from_data_dict(dd, pop_ids=['CrossRiver', 'CVLN'], projections=[14, 30], polarized=False)

popt = [0.13884385306612057, 6.898571947781088, 0.8892858357817505, 8.873353884421421, 2.639000356549434, 0.5557370050463042, 0.3119384036173035, 1.0892571235911248, 0.10875987615580193]
ns = [14, 30]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [0.13884385306612057, 6.898571947781088, 0.8892858357817505, 8.873353884421421, 2.639000356549434, 0.5557370050463042, 0.3119384036173035, 1.0892571235911248, 0.10875987615580193]
print('Drawing model to model_from_GADMA.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['CrossRiver', 'CVLN'],
	nref=1,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
