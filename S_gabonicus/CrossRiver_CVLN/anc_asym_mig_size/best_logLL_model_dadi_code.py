#current best params = [0.22378655114971516, 7.034215668609187, 11.999999984777352, 11.999999999802974, 1.8493817961481593, 0.1584141985836464, 1.1838945569082586, 0.04431962098604744]
import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "CrossRiver_CVLN/anc_asym_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = dadi.Misc.make_data_dict('data/dadi_2pops_CrossRiver_CVLN_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['CrossRiver', 'CVLN'], projections=[14, 30], polarized=False)

popt = [0.22378655114971516, 7.034215668609187, 11.999999984777352, 11.999999999802974, 1.8493817961481593, 0.1584141985836464, 1.1838945569082586, 0.04431962098604744]
popt = [     0.271,      6.826,    100.000,     54.277,      1.487,      0.154,      1.263,      0.039]
pts = [40, 50, 60]
ns = [14, 30]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))


