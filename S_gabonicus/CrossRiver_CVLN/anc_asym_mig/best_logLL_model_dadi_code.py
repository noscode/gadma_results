#current best params = [0.38185744809547506, 7.110205516293597, 1.0382882402198539, 0.16813315249278885, 1.1214829264800372, 4.351981800520521e-08]
import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "CrossRiver_CVLN/anc_asym_mig/dem_model.py")
generated_model = file_with_model_func.model_func
dd = dadi.Misc.make_data_dict('data/dadi_2pops_CrossRiver_CVLN_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['CrossRiver', 'CVLN'], projections=[14, 30], polarized=False)

popt = [0.38185744809547506, 7.110205516293597, 1.0382882402198539, 0.16813315249278885, 1.1214829264800372, 4.351981800520521e-08]
pts = [40, 50, 60]
ns = [14, 30]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))
