#current best params = [11.999999975767269, 12.0, 0.3128732333080454, 5.061160699601657, 0.16433167184736897, 0.19072240785260194]
import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "CrossRiver_CVLN/no_mig_size/dem_model.py")
generated_model = file_with_model_func.model_func
dd = dadi.Misc.make_data_dict('data/dadi_2pops_CrossRiver_CVLN_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['CrossRiver', 'CVLN'], projections=[14, 30], polarized=False)

popt = [11.999999975767269, 12.0, 0.3128732333080454, 5.061160699601657, 0.16433167184736897, 0.19072240785260194]
pts = [40, 50, 60]
ns = [14, 30]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))
