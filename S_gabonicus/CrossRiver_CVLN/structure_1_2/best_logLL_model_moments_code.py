#current best params = [250.58604010875456, 0.028338619900508375, 35.668386346583162, 1734.8174708186104, 239.82192782458628, 2259.3953417830862, 276.24870387601732, 25.457106303838547, 0.01050066855924687, 0.0, 0.002036792311857093, 0.0012892160595915341]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np
def generated_model(params, ns):
	Ns = params[:6]
	Ts = params[6:8]
	Ms = params[8:]
	theta1 = 1
	sts = moments.LinearSystem_1D.steady_state_1D(sum(ns), theta=theta1, N=Ns[0])
	fs = moments.Spectrum(sts)

	before = [Ns[0]]
	fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[0]
	after = Ns[2:4]
	growth_funcs = [lambda t: after[0], lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, params[8]],[params[9], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	T = Ts[1]
	after = Ns[4:6]
	growth_funcs = [lambda t: after[0], lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, params[10]],[params[11], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	return fs
dd = moments.Misc.make_data_dict('data/dadi_2pops_CrossRiver_CVLN_snps.txt')
data = moments.Spectrum.from_data_dict(dd, pop_ids=['CrossRiver', 'CVLN'], projections=[14, 30], polarized=False)

popt = [250.58604010875456, 0.028338619900508375, 35.668386346583162, 1734.8174708186104, 239.82192782458628, 2259.3953417830862, 276.24870387601732, 25.457106303838547, 0.01050066855924687, 0.0, 0.002036792311857093, 0.0012892160595915341]
ns = [14, 30]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll(model, data)
ll_true = moments.Inference.ll(data, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:
popt_norm = [1.0, 0.028338619900508375, 0.14233987787628974, 6.9230411640875857, 0.95704424604221117, 9.0164453726253342, 1.1024105882200188, 0.10159028129735456, 2.6313209527561741, 0.0, 0.51039171995222443, 0.32305954721765462]
print('Drawing model to model_from_GADMA.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['CrossRiver', 'CVLN'],
	nref=250,
	draw_scale=True,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
