#current best params = [250.58604010875456, 0.028338619900508375, 35.668386346583162, 1734.8174708186104, 239.82192782458628, 2259.3953417830862, 276.24870387601732, 25.457106303838547, 0.01050066855924687, 0.0, 0.002036792311857093, 0.0012892160595915341]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:6]
	Ts = params[6:8]
	Ms = params[8:]
	theta1 = 1
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1, nu=Ns[0])
	before = [Ns[0]]
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[0]
	after = Ns[2:4]
	growth_func_1 = after[0]
	growth_func_2 = after[1]
	phi = dadi.Integration.two_pops(phi, xx,  T=T,nu1=growth_func_1, nu2=growth_func_2, m12=params[8], m21=params[9], theta0=theta1)
	before = after
	T = Ts[1]
	after = Ns[4:6]
	growth_func_1 = after[0]
	growth_func_2 = after[1]
	phi = dadi.Integration.two_pops(phi, xx,  T=T,nu1=growth_func_1, nu2=growth_func_2, m12=params[10], m21=params[11], theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs
dd = dadi.Misc.make_data_dict('data/dadi_2pops_CrossRiver_CVLN_snps.txt')
data = dadi.Spectrum.from_data_dict(dd, pop_ids=['CrossRiver', 'CVLN'], projections=[14, 30], polarized=False)

popt = [250.58604010875456, 0.028338619900508375, 35.668386346583162, 1734.8174708186104, 239.82192782458628, 2259.3953417830862, 276.24870387601732, 25.457106303838547, 0.01050066855924687, 0.0, 0.002036792311857093, 0.0012892160595915341]
pts = [40, 50, 60]
ns = [14, 30]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll(model, data)
ll_true = dadi.Inference.ll(data, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
