import sys
import os 

if len(sys.argv) == 1:
    print "Please enter SFS file. For example:\n" + sys.argv[0] + 'data/dadi_2pops_Northern_Southern_snps.txt'
    os._exit(1)

spectrum_file = sys.argv[1]
spectrum_file = os.path.abspath(spectrum_file)

# Information about data: name, pop labels and dadi pts
tested_pops = { 'Northern_Southern': [['Northern', 'Southern'], ['40', '18'], ['50', '60', '70']],
                'CVLN_CVLS': [['CVLN', 'CVLS'], ['30', '18'], ['40', '50', '60']],
                'CrossRiver_CVLN': [['CrossRiver', 'CVLN'], ['14', '30'], ['40', '50', '60']]}

dirname = spectrum_file.split('/')[-1][len('dadi_2pops_'):-(len('_snps.txt'))]

if dirname not in tested_pops:
    print("File " + sys.argv[1] + " is not valid.")
    os._exit(1)
    
if not os.path.exists(dirname):
    os.makedirs(dirname)

# Observed models. Names are from models_from_dadi_pipeline.py file.
models_names = {'Northern_Southern': ['unidir_m12_sym_mig_size', 'unidir_m12_asym_mig_size'], 
                'CrossRiver_CVLN': ['unidir_m12_sym_mig_size', 'unidir_m12_asym_mig_size'], 
                'CVLN_CVLS': ['unidir_m21_sym_mig_size', 'unidir_m21_asym_mig_size']}

tested_models = {'unidir_m12_sym_mig_size':  ['n', 'n', 'n', 'n', 'm', 'm', 't', 't'], 
                'unidir_m21_sym_mig_size':  ['n', 'n', 'n', 'n', 'm', 'm', 't', 't'],
                'unidir_m12_asym_mig_size':  ['n', 'n', 'n', 'n', 'm', 'm', 'm', 't', 't'],
                'unidir_m21_asym_mig_size':  ['n', 'n', 'n', 'n', 'm', 'm', 'm', 't', 't']}

file_with_models_code = os.path.abspath('our_models_moments_code.py')

for model_name in models_names[dirname]:
    print model_name
    model_dirname = os.path.join(dirname, model_name)
    if not os.path.exists(model_dirname):
        os.makedirs(model_dirname)

    # Create file with model code
    file_with_code = os.path.join(model_dirname, 'dem_model.py') 
    with open(file_with_code, 'w') as f:
        f.write('import imp\n')
        f.write('def model_func(params, ns):\n')
        f.write('\tmodule = imp.load_source("module", "' + file_with_models_code + '")\n')
        f.write('\treturn module.' + model_name + '(params, ns)')

    # Create file with parameters for GADMA
    with open(os.path.join(model_dirname, 'params'), 'w') as f:
        f.write('Output directory: ' + os.path.join(model_dirname, 'gadma_outdir') + '\n')
        f.write('Input file: ' + spectrum_file + '\n')
        f.write('Population labels: ' + ', '.join(tested_pops[dirname][0]) + '\n')
        f.write('Projections: ' + ', '.join(tested_pops[dirname][1]) + '\n')
        f.write('Custom filename: ' + file_with_code +'\n')
        f.write('Use moments or dadi: moments\n')
        f.write('Pts : ' + ', '.join(tested_pops[dirname][2]) + '\n')
        f.write('Parameter identifiers: ' + ','.join(tested_models[model_name]) + '\n')
        f.write('Number of repeats: 10\nNumber of processes: 2\n')
        f.write('Silence: True\n')

    # Run GADMA
#    from subprocess import call
#    call(["gadma", "-p", os.path.join(model_dirname, 'params')])
