import dadi
from dadi import Numerics, PhiManip, Integration
from dadi.Spectrum_mod import Spectrum
import gadma
import sys
import numpy as np

def unidir_m12_sym_mig_size(params, ns, pts):
    """
    Split with unidirectional gene flow, followed by size change with symmetrical gene flow.

    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    m12a: Unidirectional migration from pop 2 to pop 1.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    mb: Migration between pop 1 and pop 2.
    T2: The scale time between the secondary contact and present.
    """
    nu1a, nu2a, nu1b, nu2b, m12a, mb,  T1, T2 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    phi = Integration.two_pops(phi, xx, T1, nu1a, nu2a, m12=m12a, m21=0)

    phi = Integration.two_pops(phi, xx, T2, nu1b, nu2b, m12=mb, m21=mb)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def unidir_m21_sym_mig_size(params, ns, pts):
    """
    Split with unidirectional gene flow, followed by size change with symmetrical gene flow.

    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    m21a: Unidirectional migration from pop 1 to pop 2.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    mb: Migration between pop 1 and pop 2.
    T2: The scale time between the secondary contact and present.
    """
    nu1a, nu2a, nu1b, nu2b, m21a, m12b, m21b,  T1, T2 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    phi = Integration.two_pops(phi, xx, T1, nu1a, nu2a, m12=0, m21=m21a)

    phi = Integration.two_pops(phi, xx, T2, nu1b, nu2b, m12=mb, m21=mb)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def unidir_m12_asym_mig_size(params, ns, pts):
    """
    Split with unidirectional gene flow, followed by size change with asymmetrical gene flow.

    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    m12a: Unidirectional migration from pop 2 to pop 1.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    m12b: Migration from pop 2 and pop 1.
    m21b: Migration from pop 1 and pop 2.
    T2: The scale time between the secondary contact and present.
    """
    nu1a, nu2a, nu1b, nu2b, m12a, m12b, m21b,  T1, T2 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    phi = Integration.two_pops(phi, xx, T1, nu1a, nu2a, m12=m12a, m21=0)

    phi = Integration.two_pops(phi, xx, T2, nu1b, nu2b, m12=m12b, m21=m21b)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def unidir_m21_asym_mig_size(params, ns, pts):
    """
    Split with unidirectional gene flow, followed by size change with asymmetrical gene flow.

    nu1a: Size of population 1 after split.
    nu2a: Size of population 2 after split.
    m21a: Unidirectional migration from pop 1 to pop 2.
    T1: The scaled time between the split and the secondary contact (in units of 2*Na generations).
    nu1b: Size of population 1 after time interval.
    nu2b: Size of population 2 after time interval.
    m12b: Migration from pop 2 and pop 1.
    m21b: Migration from pop 1 and pop 2.
    T2: The scale time between the secondary contact and present.
    """
    nu1a, nu2a, nu1b, nu2b, m21a, m12b, m21b,  T1, T2 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    phi = Integration.two_pops(phi, xx, T1, nu1a, nu2a, m12=0, m21=m21a)

    phi = Integration.two_pops(phi, xx, T2, nu1b, nu2b, m12=m12b, m21=m21b)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs



