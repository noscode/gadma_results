# Result best demographic models for YRI, CEU case

## Data

`YRI_CEU.fs` - from Gutenkunst et al., 2009 [1].

## Model 1

Model 1 - the same demographic model as in Gutenkunst et al 2009. Demographic model is for two populations: YRI and CEU. There are only 6 parameters:

* nu1F - the ancestral population size after growth.
* nu2B - the bottleneck size for CEU population.
* nu2F - the final size for CEU population.
*   m - the scaled migration rate.
* Tp - the scaled time between ancestral population growth and the split.
* T - the time between the split and present.

File `model_1/demographic_model_dadi.py` contains demographic model description that use dadi.

File `model_1/demographic_model_moments.py` contains demographic model description that use moments.

Parameters for GADMA are in file `model_1/params`:
```console
Input file: ../data/YRI_CEU.fs
Population labels: YRI, CEU
Output directory : gadma_results
Theta0 : 0.37976
Time for generation: 25
Custom filename : demographic_model_dadi.py
Parameter identifiers: N, N, N, m, T, T
Lower bounds: 1e-2, 1e-2, 1e-2, 0, 0, 0
Upper bounds: 100, 100, 100, 10, 3, 3
Use moments or dadi:dadi
Pts: 40,50,60
Number of repeats: 10
Number of processes: 3
```

One can run 10 runs of GADMA (in 3 threads) from `model_1` directory as follows:

```console
$ gadma -p params
```

However, one can also find result model in the directory. To test it:

```console
$ python model_1_dadi_code.py 
Model log likelihood (LL(model, data)): -1066.27619248
Optimal value of theta: 2744.18481787
```

Also we produce moments code to draw demographic model. To draw it:
```console
$ python model_1_moments_code.py 
Model log likelihood (LL(model, data)): -1066.65411214
Optimal value of theta: 2736.89371998
Drawing model to model_1.png
```

## Model 2

Model with all possible parameters for (2,1) structure. Optimization first infer demographic mode with (1,1) structure and then increase it and infer with (2,1) structure.

Parameters for GADMA are in file `model_2/params`:
```console
Output directory : YRI_CEU_2d_from_11
Input file : ../data/YRI_CEU.fs
Population labels : YRI, CEU
Projections : 20, 20
Theta0 : 0.37976
Time for generation : 25.0
Use moments or dadi : dadi
Pts : 40, 50, 60
Initial structure : 1, 1
Final structure : 2, 1

Number of repeats : 10
Number of processes : 3

#Extra parameter
min_N : 1e-3
```

One can run 10 runs of GADMA (in 3 threads) from `model_2` directory as follows:

```console
$ gadma -p params
```

Result model code is in two files: `model_2/best_model_from_simple_dadi.py` and `model_2/best_model_from_simple_moments.py`. Code with moments usage also draw model. One can test it the following way:
```console
$ python best_model_from_simple_dadi.py 
Model log likelihood (LL(model, data)): -1065.87252587
```

`result_from_simple.log` - example of GADMA output.

## Model 3

Model with all possible parameters for (2,1) structure.  Optimization infer demographic mode with (2,1) structure without increasing of the structure.

Parameters for GADMA are in file `model_3/params`:
```console
Output directory : YRI_CEU_2d_from_21
Input file : ../data/YRI_CEU.fs
Population labels : YRI, CEU
Projections : 20, 20
Theta0 : 0.37976
Time for generation : 25.0
Use moments or dadi : dadi
Pts : 40, 50, 60
Initial structure : 2, 1
Final structure : 2, 1

Number of repeats : 10
Number of processes : 3

#Extra parameter
min_N : 1e-3
```

One can run 10 runs of GADMA (in 3 threads) from `model_3` directory as follows:

```console
$ gadma -p params
```

Result model code is in two files: `model_2/best_model_from_complex_dadi.py` and `model_2/best_model_from_complex_moments.py`. Code with moments usage also draw model. One can test it the following way:
```console
$ python best_model_from_complex_dadi.py 
Model log likelihood (LL(model, data)): -1065.14883197
```

`result_from_complex.log` - example of GADMA output.


## Results of BFGS search

Moreover BFGS search of dadi (`dadi.Inference.optimize_log` function) was launched 100 times on model from Gutenkenst et al.  The results of runs are represented in file `results_of_100_runs_of_BFGS.txt`.


## References

[1] Gutenkunst, R. N., Hernandez, R. D., Williamson, S. H., and Bustamante, C. D. 2009. Inferring the joint demographic history of multiple populations from multidimensional SNP frequency data. PLoS genetics , 5(10): e1000695.
