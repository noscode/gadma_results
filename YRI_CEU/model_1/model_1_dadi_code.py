import dadi
import numpy as np

import imp
file_with_model_func = imp.load_source("file_with_model_func", "demographic_model_dadi.py")
generated_model = file_with_model_func.model_func
data = dadi.Spectrum.from_file('../data/YRI_CEU.fs')

popt = [     1.880,      0.073,      1.741,      0.933,      0.363,      0.112]
#popt = [1.881, 0.0710, 1.845, 0.911, 0.355, 0.111] # for parameters from Gutenkunst et al
pts = [40, 50, 60]
ns = [20, 20]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

from dadi import Godambe
try:

    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
        try:
            claic_score = gadma.Inference.get_claic_score(func_ex, all_boot, popt, data, pts=pts, eps=eps)
        except Exception, e:
            print('Error for eps = {0:.1e} : '.format(eps) + str(e))
            claic_score = None
            
        print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')
