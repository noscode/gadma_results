import pandas as pd
import numpy as np

df = pd.read_pickle('result_table.pkl')

data = np.array(df)

columns = ['N_{A}', 'N_{AF0}', 'N_{EU0}', 'N_{AF}', 'N_{EU}', 'm_{AF-EU}', 'm_{EU-AF}', 'T_{AF}', 'T_{AF-EU}']

new_data = np.zeros(shape=data.shape)
new_data[:,0] = data[:, -1]
new_data[:,1:5] = data[:,0:4]
new_data[:,5:7] = data[:,6:8]
new_data[:,7:9] = data[:,4:6]

new_data[:,2] = (1 - new_data[:,2]) * new_data[:,1]
for i in xrange(1, new_data.shape[1]):
	if i not in [5,6]: # migrations
		new_data[:, i] *= new_data[:, 0]
	else:
		new_data[:, i] /= 2 *  new_data[:, 0]

# time
new_data[:,-2] += new_data[:,-1]
new_data[:,-2:] *= 50

df = pd.DataFrame(new_data, index=df.index, columns=columns)

print df
df.to_pickle('result_table_normal_units.pkl')
