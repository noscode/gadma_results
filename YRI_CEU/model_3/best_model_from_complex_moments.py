#current best params = [7212.265640938217, 14028.25299995947, 0.9982596240770021, 13272.966992659245, 8942.856573598625, 2726.3356653442984, 695.0200952559259, 0.00011584854093159247, 0.00012052007995063637]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np
def generated_model(params, ns):
	Ns = params[:4]
	Ts = params[4:6]
	Ms = params[6:]
	theta1 = 0.37976
	sts = moments.LinearSystem_1D.steady_state_1D(sum(ns), theta=theta1)
	fs = moments.Spectrum(sts)

	before = [1.0]
	T = Ts[0]
	after = Ns[0:1]
	growth_funcs = [lambda t: after[0]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	fs.integrate(Npop=list_growth_funcs, tf=T, dt_fac=0.1, theta=theta1)

	before = list(after)
	fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[1]
	after = Ns[2:4]
	growth_funcs = [lambda t: after[0], lambda t: before[1] + (after[1] - before[1]) * (t / T)]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, Ms[0]],[Ms[1], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	return fs
data = moments.Spectrum.from_file('../data/YRI_CEU.fs')

popt = [1.945054951987956, 0.9982596240770021, 1.8403325187191266, 1.2399510803979874, 0.37801376170465617, 0.09636640271690176, 0.835530451313749, 0.8692228316711016]
ns = [20, 20]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = moments.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

try:
    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
        try:
            claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=eps)
        except Exception, e:
            print('Error for eps = {0:.1e} : '.format(eps) + str(e))
            claic_score = None
            
        print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))

except ImportError:
    print('Install GADMA to calculate CLAIC score')

#now we need to norm vector of params so that first value is 1:
popt_norm = [1.945054951987956, 0.9982596240770021, 1.8403325187191266, 1.2399510803979874, 0.37801376170465617, 0.09636640271690176, 0.835530451313749, 0.8692228316711016]
print('Drawing model to model_from_GADMA_from_complex.png')
model = moments.ModelPlot.generate_model(generated_model, popt_norm, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_from_GADMA_from_complex.png',
	fig_title='',
	pop_labels=['YRI', 'CEU'],
	nref=7212,
	draw_scale=True,
	gen_time=0.025,
	gen_time_units="Thousand years",
	reverse_timeline=True)
