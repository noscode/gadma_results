#current best params = [7194.792822462478, 13410.251542201073, 0.9582544565961783, 13542.979276844108, 12114.968575519626, 2683.3787253409746, 846.6668954957415, 0.00014172779289593632, 0.00012195685425105608]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:4]
	Ts = params[4:6]
	Ms = params[6:]
	theta1 = 0.37976
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1)
	before = [1.0]
	T = Ts[0]
	after = Ns[0:1]
	growth_func = after[0]
	phi = dadi.Integration.one_pop(phi, xx, nu=growth_func,T=T, theta0=theta1)
	before = list(after)
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[1]
	after = Ns[2:4]
	growth_func_1 = after[0]
	growth_func_2 = lambda t: before[1] * (after[1] / before[1]) ** (t / T)
	phi = dadi.Integration.two_pops(phi, xx,  T=T,nu1=growth_func_1, nu2=growth_func_2, m12=Ms[0], m21=Ms[1], theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs
data = dadi.Spectrum.from_file('../data/YRI_CEU.fs')

popt = [1.8638829321580523, 0.9582544565961783, 1.8823306815120924, 1.6838523185401713, 0.3729612223111333, 0.1176777311575127, 1.0197021070711312, 0.8774542996156008]
pts = [40, 50, 60]
ns = [20, 20]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

try:
    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
        try:
            claic_score = gadma.Inference.get_claic_score(func_ex, all_boot, popt, data, pts=pts, eps=eps)
        except Exception, e:
            print('Error for eps = {0:.1e} : '.format(eps) + str(e))
            claic_score = None
            
        print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))

except ImportError:
    print('Install GADMA to calculate CLAIC score')
