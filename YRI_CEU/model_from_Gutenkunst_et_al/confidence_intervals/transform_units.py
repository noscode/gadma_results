import pandas as pd
import numpy as np

df = pd.read_pickle('result_table.pkl')

data = np.array(df)

columns = ['N_{A}', 'N_{AF}', 'N_{EU0}',  'N_{EU}', 'm_{AF-EU}', 'T_{AF}', 'T_{AF-EU}']

new_data = np.zeros(shape=data.shape)
new_data[:,0] = data[:, -1]
new_data[:,1:] = data[:,:-1]

new_data[:, 0] /= 0.37976
for i in xrange(1, new_data.shape[1]):
	if i not in [4]: # migrations
		new_data[:, i] *= new_data[:, 0]
	else:
		new_data[:, i] /= 2 *  new_data[:, 0]

# time
new_data[:,-2] += new_data[:,-1]
new_data[:,-2:] *= 50

df = pd.DataFrame(new_data, index=df.index, columns=columns)

print df
df.to_pickle('result_table_normal_units.pkl')
