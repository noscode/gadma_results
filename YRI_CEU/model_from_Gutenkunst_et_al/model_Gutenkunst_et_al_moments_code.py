import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np
from demographic_model_moments import model_func

data = moments.Spectrum.from_file('../data/YRI_CEU.fs')

popt = [1.881, 0.0710, 1.845, 0.911, 0.355, 0.111] # for parameters from Gutenkunst et al
ns = [20, 20]
model = model_func(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = moments.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

try:
    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../data/bootstraps')
    for eps in [1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]:
        try:
            claic_score = gadma.Inference.get_claic_score(model_func, all_boot, popt, data, pts=None, eps=eps)
        except Exception, e:
            print('Error for eps = {0:.1e} : '.format(eps) + str(e))
            claic_score = None
            
        print('Model Composite Likelihood AIC score (CLAIC(p0, eps={0:.1e})): {1}'.format(eps, claic_score))

except ImportError:
    print('Install GADMA to calculate CLAIC score')

#now we need to norm vector of params so that first value is 1:
theta0 = 0.37976
print('Drawing model to model_1.png')
model = moments.ModelPlot.generate_model(model_func, popt, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_1.png',
	fig_title='Demographic model from GADMA',
	pop_labels=['YRI', 'CEU'],
	nref=int(theta / theta0),
	draw_scale=True,
	gen_time=0.025,
	gen_time_units="Thousand years",
	reverse_timeline=True)
