import dadi
import numpy as np

from demographic_model import model_func

data = dadi.Spectrum.from_file('../data/puma.fs')
#data = data.fold()
pts = [40, 50, 60]
ns = data.sample_sizes

p0 = [0.14197057087635756, 0.007278547308962829, 0.020920390519867927, 0.008188999725483698]
func_ex = dadi.Numerics.make_extrap_log_func(model_func)
model = func_ex(p0, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

mu = 2.2e-9
L = 2564692624
theta0 = 4 * mu * L
Nanc = int(theta / theta0) # 119254
print('Size of ancestral population: {0}'.format(Nanc))

import demes
import demesdraw

graph = demes.load("demes_model_1.yml")
w = demesdraw.utils.separation_heuristic(graph)
positions = dict(Ancestral=0, Texas=0, Florida=w)
fig, ax = demesdraw.utils.get_fig_axes(aspect=0.7, scale=0.6)
ax = demesdraw.tubes(graph, ax=ax, log_time=True, positions=positions)
ax.figure.savefig("puma_model1.pdf")
