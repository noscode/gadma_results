import dadi
import numpy as np

from demographic_model import model_func

data = dadi.Spectrum.from_file('../data/cabbage.fs')
pts = [100, 110, 120]
ns = data.sample_sizes

p0 = [4.970607502035091, 0.0005565445599786766, 0.1602550455634176, 1.610768792150505e-05]
func_ex = dadi.Numerics.make_extrap_log_func(model_func)
model = func_ex(p0, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

mu = 1.5e-8
L = 411560319
theta0 = 4 * mu * L
Nanc = int(theta / theta0) # 119254
print('Size of ancestral population: {0}'.format(Nanc))


import demes
import demesdraw

graph = demes.load("demographic_model_1.yml")
w = demesdraw.utils.separation_heuristic(graph)
fig, ax = demesdraw.utils.get_fig_axes(aspect=0.7, scale=0.6)
ax = demesdraw.tubes(graph, ax=ax, log_time=False)
ax.figure.savefig("cabbage_model1.pdf")
