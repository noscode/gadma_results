import dadi
import numpy as np

from demographic_model import model_func

data = dadi.Spectrum.from_file('../data/cabbage.fs')
pts = [100, 110, 120]
ns = data.sample_sizes

p0 = [1.8171148402808621, 10000.0, 0.4730258313202693, 0.007333849303507127, 0.5779941515668624]
func_ex = dadi.Numerics.make_extrap_log_func(model_func)
model = func_ex(p0, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

mu = 1.5e-8
L = 411560319
theta0 = 4 * mu * L
Nanc = int(theta / theta0) # 119254
print('Size of ancestral population: {0}'.format(Nanc))

import demes
import demesdraw

graph = demes.load("demographic_model_2.yml")
w = demesdraw.utils.separation_heuristic(graph)
fig, ax = demesdraw.utils.get_fig_axes(aspect=0.7, scale=0.6)
ax = demesdraw.tubes(graph, ax=ax, log_time=False)
ax.figure.savefig("cabbage_model2.pdf")
