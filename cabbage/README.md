# Domesticated cabbage

One population

## Data

- `cabbage.fs` - folded SFS data from Blischak et al. 2020 [1].
- `boot` - block bootstrap SFS data from Blischak et al. 2020 [1].

## Models

- Model 1 - demographic model without inbreeding from Blischak et al. 2020.
- Model 2 - demographic model with inbreeding from Blischak et al. 2020.

Each directory contains:

- `demographic_model.py` - `dadi` code for the demographic model.
- `model_X.py` - best values of demographic parameters obtained with GADMA2 for model `X`.
- `get_confidence_intervals.py` - script that evaluates confidence intervals for the model parameters (based on scripts from Blischak et al. 2020).
- `demographic_model_X.yml` - `demes` description of best demographic history.
- `cabbage_model_X.pdf` - picture of the demographic history from `demes`.
- `gadma2_100runs.csv` - CSV file with results of 100 GADMA2 runs with parameter bounds from Blischak et al. 2020. The result models that are reported were inferred with wider bounds.

## References

[1] Paul D Blischak, Michael S Barker, Ryan N Gutenkunst, Inferring the Demographic History of Inbred Species from Genome-Wide SNP Frequency Data, Molecular Biology and Evolution, Volume 37, Issue 7, July 2020, Pages 2124–2136, https://doi.org/10.1093/molbev/msaa042
