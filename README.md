# Welcome

This repo contains several results for GADMA software, that can be found [here](https://github.com/ctlab/GADMA).

GADMA  is tool for inferring demographic history from allele-frequency spectrum. It was implemented by Ekaterina Noskova.

GADMA was tested on several popular cases: for human populations, butterfly *Euphydryas gilletti* and Gaboon forest frog *Scotobleps gabonicus*. **UPDATE: Results for American puma and domsticated cabbage from GADMA2 are also presented here. The human, buterflies and forest frogs scripts were not changed so they could be run only with GADMA version 1 and Python2. Scripts for puma and cabbage require GADMA2 and Python3.**

YRI, CEU, CHB are three most popular populations to infer demographic history. One of the papers that are  devoted to this is "Inferring the Joint Demographic History of Multiple Populations from Multidimensional SNP Frequency Data" (Gutenkunst et al., 2009). In Supplementary there is model for two populations: YRI and CEU, and model for all three populations is introduced in the paper.

Demographic history of butterflies *E.gilletti* was studied in "Genomic inference accurately predicts the timing and severity of a recent bottleneck in a non-model insect population" (McCoy et al., 2013). Data from the paper was taken from [avaliable]( https://github.com/rmccoy7541/egillettii-rnaseq/tree/master/data).

Demographic model selection for Gaboon forest frog *Scotobleps gabonicus* was performed in "Evaluating mechanisms of diversification in a Guineo-Congolian tropical forest frog using demographic model selection" (Portik et al., 2017).

Data and demographic models for American puma and domesticated cabbage were originally presented in the paper "Inferring the Demographic History of Inbred Species from Genome-Wide SNP Frequency Data" (Blischak et al. 2020).

## YRI, CEU

GADMA was tested on inferring two basic demographic models:

* model 1 - the same model as in Gutenkunst et al. (6 parameters).
* model 2 and 3 - models with all avaliable parameters (12 parameters).

Models 2 and 3 have the same parameters and differ in optimization pipeline only.

Also for the same demographic model as in the paper was used BFGS method from dadi.

More information [here](https://bitbucket.org/noscode/gadma_results/src/master/YRI_CEU).

## YRI, CEU, CHB

For three human populations we inferred three demographic models:

* Model 1 - the same as in Guntenkunst et al. (13 parameters).
* Model 2 - the same as in Guntenkunst et al. (14 parameters) with extra  upper bound on time of exantion out of Africa (150 kya ago).
* Model 3 - with all possible parameters (26) and same extra upper bound as in model 2.

More information [here](https://bitbucket.org/noscode/gadma_results/src/master/YRI_CEU_CHB).

## Butterflies

We inferred demographic model with all possible parameters and receive different local optimas close to global optimum.

More information [here](https://bitbucket.org/noscode/gadma_results/src/master/E_gillettii).

## Forest frogs

We repeat the same model selection as it was performed in Portik et al. Then we inferred demographic models with all possible parameters and based on the result add two new models in observed models in model selection. New best by AIC score models were recieved.

More information [here](https://bitbucket.org/noscode/gadma_results/src/master/S_gabonicus).

## American puma

**Results for GADMA2**

Two demographic models were inferred: with and without inbreeding.

More information [here](https://bitbucket.org/noscode/gadma_results/src/master/puma).

## Domesticated cabbage

**Results for GADMA2**

Two demographic models were inferred: with and without inbreeding.

More information [here](https://bitbucket.org/noscode/gadma_results/src/master/cabbage).

