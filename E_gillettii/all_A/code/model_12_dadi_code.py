#current best params = [1393.7770481492787, 0.99999, 2041.9375052995338, 232.74370039741243, 156.06665179613867]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:3]
	Ts = params[3:4]
	theta1 = 1
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1)
        before = [1.0]
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	before.append((1 - Ns[0]) * before[-1])
	before[-2] *= Ns[0]
	T = Ts[0]
	after = Ns[2:4]
	growth_func_1 = lambda t: before[0] + (after[0] - before[0]) * (t / T)
	growth_func_2 = after[1]
	phi = dadi.Integration.two_pops(phi, xx,  T=T, nu1=growth_func_1, nu2=growth_func_2, m12=0, m21=0, theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs

data = dadi.Spectrum.from_file('../../data/result_all.fs')

popt = [0.99999, 1.4650388367428724, 0.16698775511223996, 0.11197389998879027]
pts = [20, 30, 40]
ns = [12, 12]

func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

try:
    import gadma
    claic_score = gadma.Inference.get_claic_score(generated_model, popt, data, pts=pts, eps=1e-14)
    print('Model Composite Likelihood AIC score (CLAIC(p0)): {0}'.format(claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')
