#current best params = [1357.690839940794, 0.5841203784510872, 1843.6110769216325, 121.16970848743189, 186.72870010487887]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:3]
	Ts = params[3:4]
	theta1 = 1
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1)
	before = [1.0]
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	before.append((1 - Ns[0]) * before[-1])
	before[-2] *= Ns[0]
	T = Ts[0]
	after = Ns[1:3]
	growth_func_1 = after[0]
	growth_func_2 = lambda t: before[1] + (after[1] - before[1]) * (t / T)
	phi = dadi.Integration.two_pops(phi, xx,  T=T, nu1=growth_func_1, nu2=growth_func_2, m12=0, m21=0, theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs

data = dadi.Spectrum.from_file('../../data/result_all.fs')

popt = [0.5841203784510872, 1.3579019778921309, 0.08924690726551257, 0.1375340354458175]
pts = [20, 30, 40]
ns = [12, 12]

func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

try:
    import gadma
    claic_score = gadma.Inference.get_claic_score(generated_model, popt, data, pts=pts, eps=1e-14)
    print('Model Composite Likelihood AIC score (CLAIC(p0)): {0}'.format(claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')
