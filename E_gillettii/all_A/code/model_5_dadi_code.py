#current best params = [1383.4197934760634, 0.4293250627706215, 1789.7475262528922, 237.11775440143256, 159.4195464852982]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:2]
        Ts = params[2:]
	theta1 = 1
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1)
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	T = Ts[0]
	after = Ns
	growth_func_1 = after[0]
	growth_func_2 = after[1]
	phi = dadi.Integration.two_pops(phi, xx,  T=T, nu1=growth_func_1, nu2=growth_func_2, m12=0, m21=0, theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs

data = dadi.Spectrum.from_file('../../data/result_all.fs')

popt = [1.2937125337464381, 0.1713997121623049, 0.11523584326109076]
pts = [20, 30, 40]
ns = [12, 12]

func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

try:
    import gadma
    claic_score = gadma.Inference.get_claic_score(generated_model, popt, data, pts=pts, eps=1e-14)
    print('Model Composite Likelihood AIC score (CLAIC(p0)): {0}'.format(claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')
