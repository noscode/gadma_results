# Result models without migrations for 50 launches on 12x12 AFS from all SNP's

Output log file one can find here: `GADMA.log`, there are all 50 models with parameters. To run GADMA:
```console
$ gadma -p params
```

Result models were classified into 5 classes. In each class there are models with close parameters.

For each class we take best by log likelihood model :

|Number of launch | log likelihood | size of class |
|----|----|----|
|20 | -277.82 | 16 from 50 |
|8  | -278.93 | 1 from 50  |
|23 | -282.88 | 10 from 50 |
|5  | -283.53 | 17 from 50 |
|12 | -284.81 | 6 from 50  |

AFS was simulated with *moments*.

## Code of models

One can find all code in `code` directory.

One can run any code to get log likelihood or/and to draw picture:

`$ python model_23_dadi_code.py`

(dadi code only perform log likelihood equation, but moments code draw model as well)

## Pictures

![`model_20`](pictures/model_20.png)
![`model_8`](pictures/model_8.png)

![`model_23`](pictures/model_23.png)
![`model_5`](pictures/model_5.png)

![`model_12`](pictures/model_12.png)

One can find pictures in `pictures` directory.

# All models

Split % is the percentage in which the ancestor population is divided. When population changes laws are sudden only, it is marked as NA (does not affect anything).

nu\_wy and nu\_co are size of populations from Wyoming and Colorado respectively.

In parentheses after the size of the population there is a law of population change:

- s = sudden
- l = linear
- e = exponential

Migration m\_1-2 is the fraction of individuals in population 1, that migrated from population 2.

Population sizes are reported with respect to an ancestral population, that is  set to nu\_A = 1, time is reported in units of 2 \* nu\_A, and migration rates in units of 1 / 2 \* nu\_A

|Model N | logLL *moments* | logLL *dadi* | split % | nu\_wy | nui\_co | t\_SPLIT |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| **20** | **-277.82** | **-277.71** | **58.41** | **1.358 (s)** | **0.089 (l)** | **0.138** |
| 4 | -277.82 | -277.71 | 58.54 | 1.358 (s) | 0.090 (l) | 0.138 |
| 31 | -277.83 | -277.73 | 59.23 | 1.354 (s) | 0.090 (l) | 0.137 |
| 10 | -277.86 | -277.77 | 60.25 | 1.353 (s) | 0.093 (l) | 0.136 |
| 38 | -277.86 | -277.77 | 60.27 | 1.354 (s) | 0.093 (l) | 0.136 |
| 30 | -277.88 | -277.79 | 60.81 | 1.353 (s) | 0.094 (l) | 0.136 |
| 6 | -277.88 | -277.80 | 61.06 | 1.350 (s) | 0.094 (l) | 0.135 |
| 7 | -277.91 | -277.84 | 61.70 | 1.349 (s) | 0.096 (l) | 0.135 |
| 34 | -277.92 | -277.85 | 61.86 | 1.348 (s) | 0.096 (l) | 0.134 |
| 48 | -277.94 | -277.88 | 62.34 | 1.347 (s) | 0.097 (l) | 0.134 |
| 24 | -277.96 | -277.90 | 62.68 | 1.347 (s) | 0.097 (l) | 0.134 |
| 13 | -277.98 | -277.93 | 63.09 | 1.346 (s) | 0.098 (l) | 0.133 |
| 46 | -278.00 | -277.95 | 63.33 | 1.343 (s) | 0.099 (l) | 0.133 |
| 26 | -278.04 | -278.00 | 63.94 | 1.342 (s) | 0.100 (l) | 0.133 |
| 32 | -278.07 | -278.03 | 64.24 | 1.343 (s) | 0.102 (l) | 0.133 |
| 50 | -278.19 | -278.16 | 65.68 | 1.334 (s) | 0.103 (l) | 0.131 |
| **8** | **-278.93** | **-278.94** | **64.33** | **1.328 (s)** | **0.119 (e)** | **0.129** |
| **23** | **-282.88** | **-282.96** | **70.73** | **1.852 (l)** | **0.107 (l)** | **0.121** |
| 49 | -282.89 | -282.95 | 69.99 | 1.862 (l) | 0.105 (l) | 0.121 |
| 9 | -282.91 | -283.00 | 72.16 | 1.819 (l) | 0.111 (l) | 0.119 |
| 16 | -282.95 | -283.05 | 72.90 | 1.809 (l) | 0.114 (l) | 0.119 |
| 15 | -283.08 | -283.20 | 74.59 | 1.776 (l) | 0.119 (l) | 0.117 |
| 27 | -283.08 | -283.20 | 74.62 | 1.770 (l) | 0.119 (l) | 0.117 |
| 33 | -283.11 | -283.23 | 74.87 | 1.769 (l) | 0.120 (l) | 0.117 |
| 36 | -283.17 | -283.29 | 75.32 | 1.759 (l) | 0.121 (l) | 0.117 |
| 2 | -283.29 | -283.43 | 76.16 | 1.743 (l) | 0.125 (l) | 0.116 |
| 17 | -283.47 | -283.61 | 77.23 | 1.718 (l) | 0.128 (l) | 0.115 |
| **5** | **-283.53** | **-283.58** | **NA** | **1.294 (s)** | **0.171 (s)** | **0.115** |
| 11 | -283.53 | -283.58 | NA | 1.294 (s) | 0.171 (s) | 0.115 |
| 44 | -283.53 | -283.58 | NA | 1.294 (s) | 0.171 (s) | 0.115 |
| 39 | -283.53 | -283.58 | NA | 1.293 (s) | 0.171 (s) | 0.115 |
| 21 | -283.53 | -283.58 | NA | 1.293 (s) | 0.171 (s) | 0.115 |
| 45 | -283.53 | -283.58 | NA | 1.293 (s) | 0.172 (s) | 0.115 |
| 19 | -283.53 | -283.58 | NA | 1.293 (s) | 0.171 (s) | 0.115 |
| 18 | -283.53 | -283.58 | NA | 1.296 (s) | 0.172 (s) | 0.115 |
| 47 | -283.53 | -283.58 | NA | 1.294 (s) | 0.172 (s) | 0.115 |
| 22 | -283.53 | -283.58 | NA | 1.293 (s) | 0.172 (s) | 0.115 |
| 3 | -283.53 | -283.58 | NA | 1.294 (s) | 0.172 (s) | 0.115 |
| 29 | -283.53 | -283.58 | NA | 1.291 (s) | 0.171 (s) | 0.115 |
| 40 | -283.54 | -283.58 | NA | 1.294 (s) | 0.172 (s) | 0.115 |
| 43 | -283.54 | -283.58 | NA | 1.292 (s) | 0.171 (s) | 0.115 |
| 1 | -283.54 | -283.58 | NA | 1.291 (s) | 0.171 (s) | 0.115 |
| 37 | -283.54 | -283.58 | NA | 1.292 (s) | 0.171 (s) | 0.115 |
| 42 | -283.54 | -283.59 | NA | 1.291 (s) | 0.171 (s) | 0.115 |
| **12** | **-284.81** | **-284.93** | **100.00** | **1.465 (l)** | **0.167 (s)** | **0.112** |
| 28 | -284.81 | -284.94 | 100.00 | 1.466 (l) | 0.167 (s) | 0.112 |
| 41 | -284.81 | -284.93 | 100.00 | 1.466 (l) | 0.167 (s) | 0.112 |
| 25 | -284.81 | -284.94 | 100.00 | 1.467 (l) | 0.167 (s) | 0.112 |
| 35 | -284.81 | -284.93 | 100.00 | 1.467 (l) | 0.167 (s) | 0.112 |
| 14 | -284.81 | -284.93 | 100.00 | 1.471 (l) | 0.168 (s) | 0.112 |

