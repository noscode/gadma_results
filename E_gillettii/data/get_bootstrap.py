import os
from collections import defaultdict
import moments
import random

def get_boot(fs_txt_filename, proj, pop_labels, out_dir):
    # 1. Get contigs information
    contig_snps = defaultdict(list)
    with open(fs_txt_filename) as f:
        first_line = f.next() # skip the first line and remember it
        for line in f:
            # name of the contig is on the -2 position
            contig_name = line.split()[-2]
            contig_snps[contig_name].append(line)
   
    # 2. Number of contigs
    n_contigs = len(contig_snps)

    # 3. Bottstrap over contigs
    all_boots = list()
    for i in xrange(100):
        contigs_in_boot = []
        for _ in xrange(n_contigs):
            ii = random.choice(xrange(n_contigs))
            contigs_in_boot.append(contig_snps.keys()[ii])
        # Write each file in txt format
        out_txt_filename = os.path.join(out_dir, str(i).zfill(2) + '.txt')
        with open(out_txt_filename, 'w') as f:
            f.write(first_line)
            for contig_name in contigs_in_boot:
                for snp in contig_snps[contig_name]:
                    splitted_snp = snp.split()
                    splitted_snp[-2] = str(ii)
                    line = '\t'.join(splitted_snp)
                    f.write(line + '\n')
        # Read as fs and write to fs file
        dd = moments.Misc.make_data_dict(out_txt_filename)
        data = moments.Spectrum.from_data_dict(dd, projections=proj, pop_ids=pop_labels, polarized=False)
        data.to_file(os.path.join(out_dir, str(i).zfill(2) + '.fs'))
        all_boots.append(data)
    return all_boots


get_boot('gillettii_data_all.AN24.dadi', (12,12), ['WY','CO'], os.path.join('bootstraps', 'all'))
get_boot('gillettii_data_syn.AN24.dadi', (12,12), ['WY','CO'], os.path.join('bootstraps', 'syn'))
