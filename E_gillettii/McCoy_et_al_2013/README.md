## Demographic models from McCoy et al. 2013

In the paper ''Genomic inference accurately predicts the timing and severity of a recent bottleneck in a nonmodel insect population'' two populations of  *E. gillettii* butterflies are represented:

* WY - natural population of butterflies in Wyoming,
* CO - butterflies from Colorado.

Second population was isolated from the natural one.

For each population eight individuals were chosen and their RNA was sequenced. After assembling and SNP calling, 2 allele-frequency spectra for *E. gillettii* butterflies were build: from synonimous SNP's and from all SNP's.

3 types of demographic models were inferred for AFS from synonimous SNP's: type A - without migrations, type B1 - with unidirected migration from CO to WY and type B2 - with all migrations. For AFS from all SNP's only model of type A was inferred.

You can find all models with found parameters in the `code` directory. Types of models, AFS and package to simulate AFS (dadi or moments) are reflected in the file names. For example, file [model_A_all_dadi_code.py](https://bitbucket.org/noscode/gadma_results/src/master/e_gillettii/McCoy_et_al_2013/code/model_A_all_dadi_code.py) corresponds to demographic model of type A for allele-frequency spectrum build from all SNP's using dadi package.

To calculate log likelihood of model one can write:

```console
$ python model_B2_syn_dadi_code.py
```

Moreover files, that uses moments library, draw model during run. One can find already drawn models in the `pictures` directory or simply run in the `code` directory:


```console
$ python model_B1_syn_moments_code.py
```
