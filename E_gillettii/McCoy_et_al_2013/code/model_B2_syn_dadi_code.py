import dadi
def generated_model(params, ns, pts):
    nuW, nuC, T, m12, m21 = params
    xx = dadi.Numerics.default_grid(pts)

    phi = dadi.PhiManip.phi_1D(xx)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)

    phi = dadi.Integration.two_pops(phi, xx, T, nuW, nuC, m12=m12, m21=m21)

    model_sfs = dadi.Spectrum.from_phi(phi, ns, (xx,xx))
    return model_sfs

data = dadi.Spectrum.from_file('../../data/result_syn.fs')

popt = [0.892, 0.122, 0.081, 0.896, 4.5e-4]
pts = [32, 42, 52]
ns = [12, 12]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
