import matplotlib
matplotlib.use('Agg')
import moments
import numpy as np
def generated_model(params, ns):
    nuW, nuC, T = params
    sfs = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sfs)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

    fs.integrate(Npop=[nuW, nuC], tf=T, dt_fac=0.1)
    return fs

data = moments.Spectrum.from_file('../../data/result_all.fs')

popt = [1.320, 0.173, 0.117]
ns = [12, 12]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
#now we need to norm vector of params so that first value is 1:

try:
    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../../data/bootstraps/all')
    claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=1e-15)
    print('Model Composite Likelihood AIC score (CLAIC(p0)): {0}'.format(claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')

print('Drawing model to model_A_all.png')
model = moments.ModelPlot.generate_model(generated_model, popt, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_A_all.png',
	fig_title='Model A (all) for Euphydryas gillettii, logLL: %.2f' % ll_model,
	pop_labels=['WY', 'CO'],
	nref=None,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
