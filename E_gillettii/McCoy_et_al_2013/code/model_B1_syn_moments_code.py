import matplotlib
matplotlib.use('Agg')
import moments
import numpy as np
def generated_model(params, ns):
    nuW, nuC, T, m12 = params
    sfs = moments.LinearSystem_1D.steady_state_1D(sum(ns))
    fs = moments.Spectrum(sfs)

    fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))
    
    m = np.array([[0, m12], [0, 0]])
    fs.integrate(Npop=[nuW, nuC], tf=T, m=m, dt_fac=0.1)
    return fs

data = moments.Spectrum.from_file('../../data/result_syn.fs')

popt = [0.884, 0.119, 0.080, 0.887]
ns = [12, 12]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
try:
    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../../data/bootstraps/syn')
    claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=1e-13)
    print('Model Composite Likelihood AIC score (CLAIC(p0)): {0}'.format(claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')

print('Drawing model to model_B1_syn.png')
model = moments.ModelPlot.generate_model(generated_model, popt, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_B1_syn.png',
	fig_title='Model B1 (syn) for Euphydryas gillettii, logLL: %.2f' % ll_model,
	pop_labels=['WY', 'CO'],
	nref=None,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
