#current best params = [1313.5142570803432, 0.0008533136002769671, 1646.8405591254677, 155.63188577992548, 272.9537641210461, 0.0007253931908882588, 0.00011500587230201578]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:4]
	Ts = params[4:5]
	Ms = params[5:]
	theta1 = 1
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1, nu=Ns[0])
	before = [Ns[0]]
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[0]
	after = Ns[2:4]
	growth_func_1 = after[0]
	growth_func_2 = lambda t: before[1] * (after[1] / before[1]) ** (t / T)
	phi = dadi.Integration.two_pops(phi, xx,  T=T, nu1=growth_func_1, nu2=growth_func_2, m12=params[5], m21=params[6], theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs
data = dadi.Spectrum.from_file('../../data/result_all.fs')

popt = [1313.5142570803432, 0.0008533136002769671, 1646.8405591254677, 155.63188577992548, 272.9537641210461, 0.0007253931908882588, 0.00011500587230201578]
pts = [32, 42, 52]
ns = [12, 12]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll(model, data)
ll_true = dadi.Inference.ll(data, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
