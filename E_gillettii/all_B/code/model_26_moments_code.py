#current best params = [1376.3816403366545, 0.939021000647867, 1736.0930430887786, 251.82924919189642, 177.92164335694142, 0.00022961098667520093, 6.586923582711419e-05]
import matplotlib
matplotlib.use('Agg')
import moments
import numpy as np
def generated_model(params, ns):
	Ns = params[:2]
	Ts = params[2:3]
	Ms = params[3:]
	theta1 = 1
	sts = moments.LinearSystem_1D.steady_state_1D(sum(ns), theta=theta1, N=1.0)
	fs = moments.Spectrum(sts)

	before = [1.0]
	fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

	T = Ts[0]
	after = Ns[0:2]
	growth_funcs = [lambda t: after[0], lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, params[3]],[params[4], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	return fs
data = moments.Spectrum.from_file('../../data/result_all.fs')

popt = [1.2613456850994764, 0.18296469657231154, 0.12926766686122235, 0.31603234647933076, 0.09066120685544536]
ns = [12, 12]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
try:
    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../../data/bootstraps/all')
    claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=1e-13)
    print('Model Composite Likelihood AIC score (CLAIC(p0)): {0}'.format(claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')

print('Drawing model to model_26.png')
model = moments.ModelPlot.generate_model(generated_model, popt, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_26.png',
	fig_title='Model 26 for Euphydryas gillettii, logLL: %.2f' % ll_model,
	pop_labels=['WY', 'CO'],
	nref=None,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
