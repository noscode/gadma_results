# Confidence intervals

Here are results for confidence intervals. Bootstrapped data is located `../../data/bootstraps/all`. For every 100 spectrum from bootrstrapped data local search (Powell method) from optimal parameters was launched. One can repeat launch by (example for model_2):

```console
$ gadma-run_ls_on_boot_data -b ../../data/bootstraps/all -o out_dir -j 4 --opt powell -p ls_params -d ../code/model_2_moments_code.py
```

where `-j 4` corresponds to 4 threads and file `ls_params` contains parameters for local search such as `lower_bound` and `upper_bound`.

* `model_*_result/result_table.csv`
* `model_*_result/result_table.pkl`
- the results of the script.

To get confidence intervals one should type:

```console
$ gadma-get_confidence_intervals model_2_results/result_table.pkl --acc 3
```

Example output:
```console
nu_{WY0}:	-0.132	0.254
nu_{WY}:	1.052	1.494
nu_{CO}:	0.050	0.105
T:			0.190	0.312
m_{WY-CO}:	0.515	1.637
m_{CO-WY}:	-0.164	0.362
```
