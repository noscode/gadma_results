# Butterfly *Euphydryas gillettii*

In the paper ''Genomic inference accurately predicts the timing and severity of a recent bottleneck in a nonmodel insect population'' two populations of  *E. gillettii* butterflies are represented [1]:

* WY - natural population of butterflies in Wyoming,
* CO - butterflies from Colorado.

Second population was isolated from the natural one.

Here we introduce results from the paper and our results using GADMA software.

There were observed two allele-frequency spectra:

* from synonimous SNP's.
* from all SNP's.

And three types of demographic models:

* A - without migrations.
* B1 - with unidirected migration from CO to WY.
* B2 - with all migrations.

GADMA was launched 50 times for two types of allele-frequncy spectra and two types (A and B2) of demographic models.

* `data` - AFS from synonimous and from all SNP's from [1].
* `McCoy_et_al_2013`  - demographic models from the paper [1].
* `all_A` - GADMA results of AFS from all SNP's for model A.
* `all_B` - GADMA results of AFS from all SNP's for model B2.
* `syn_A` - GADMA results of AFS from synonimous SNP's for model A.
* `syn_B` - GADMA results of AFS from synonimous SNP's for model B2.


To run GADMA for one of four cases, please run from the corresponding directory the following:
```console
$ gadma -p params
```

Moreover each directory contain GADMA output example `GADMA.log`.

## References

[1] McCoy, R. C., Garud, N. R., Kelley, J. L., Boggs, C. L., and Petrov, D. A. 2014. Genomic inference accurately predicts the timing and severity of a recent bottleneck in a nonmodel insect population. Molecular ecology, 23(1): 136–150.
