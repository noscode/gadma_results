#current best params = [545.3556214719605, 0.99999, 404.8484087058085, 65.50825428334436, 43.4816836917782, 0.0017466545422718828, 0.0]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:4]
	Ts = params[4:5]
	Ms = params[5:]
	theta1 = 1
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1, nu=Ns[0])
	before = [Ns[0]]
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[0]
	after = Ns[2:4]
	growth_func_1 = lambda t: before[0] + (after[0] - before[0]) * (t / T)
	growth_func_2 = after[1]
	phi = dadi.Integration.two_pops(phi, xx,  T=T, nu1=growth_func_1, nu2=growth_func_2, m12=params[5], m21=params[6], theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs
data = dadi.Spectrum.from_file('/Users/pdobrynin/enoskova/butterfly/data/result_syn.fs')

popt = [545.3556214719605, 0.99999, 404.8484087058085, 65.50825428334436, 43.4816836917782, 0.0017466545422718828, 0.0]
pts = [32, 42, 52]
ns = [12, 12]
func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll(model, data)
ll_true = dadi.Inference.ll(data, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
