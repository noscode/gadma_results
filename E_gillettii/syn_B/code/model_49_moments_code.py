#current best params = [545.3556214719605, 0.99999, 404.8484087058085, 65.50825428334436, 43.4816836917782, 0.0017466545422718828, 0.0]
import matplotlib
matplotlib.use('Agg')
import moments
import numpy as np
def generated_model(params, ns):
	Ns = params[:3]
	Ts = params[3:4]
	Ms = params[4:]
	theta1 = 1
	sts = moments.LinearSystem_1D.steady_state_1D(sum(ns), theta=theta1, N=1.0)
	fs = moments.Spectrum(sts)

	before = [1.0]
	fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

	before.append((1 - Ns[0]) * before[-1])
	before[-2] *= Ns[0]
	T = Ts[0]
	after = Ns[1:3]
	growth_funcs = [lambda t: before[0] + (after[0] - before[0]) * (t / T), lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
	m = np.array([[0, params[4]],[params[5], 0]])
	fs.integrate(Npop=list_growth_funcs, tf=T, m=m, dt_fac=0.1, theta=theta1)

	before = after
	return fs
data = moments.Spectrum.from_file('../../data/result_syn.fs')

popt = [0.99999, 0.7423567169126977, 0.12012025127114687, 0.0797308801446247, 0.9525478733975054, 0.0]
ns = [12, 12]
model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
try:
    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../../data/bootstraps/syn')
    claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=1e-11)
    print('Model Composite Likelihood AIC score (CLAIC(p0)): {0}'.format(claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')

print('Drawing model to model_49.png')
model = moments.ModelPlot.generate_model(generated_model, popt, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_49.png',
	fig_title='Model 49 for Euphydryas gillettii, logLL: %.2f' % ll_model,
	pop_labels=['WY', 'CO'],
	nref=None,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
