# Confidence intervals

Here are results for confidence intervals. Bootstrapped data is located `../../data/bootstraps/syn`. For every 100 spectrum from bootrstrapped data local search (Powell method) from optimal parameters was launched. One can repeat launch by (example for model_24):

```console
$ gadma-run_ls_on_boot_data -b ../../data/bootstraps/syn/ -o out_dir -j 4 --opt powell -p ls_params -d ../code/model_24_moments_code.py
```

where `-j 4` corresponds to 4 threads and file `ls_params` contains parameters for local search such as `lower_bound` and `upper_bound`.

* `model_*_result/result_table.csv`
* `model_*_result/result_table.pkl`
- the results of the script.

To get confidence intervals one should type:

```console
$ gadma-get_confidence_intervals model_24_results/result_table.pkl --acc 3
```

Example output:
```console
nu_{WY0}:	0.671	0.898
nu_{WY}:	0.693	1.186
nu_{CO}:	0.035	0.092
T:			0.052	0.098
Theta:		225.859	254.269
```
