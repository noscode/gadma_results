# Result models without migrations for 50 launches on 12x12 AFS from synonymous SNP's

Output log file one can find here: `GADMA.log`, there are all 50 models with parameters. To run GADMA:
```console
$ gadma -p params
```

 4 main classes were extracted from result models. In each class there are models with close parameters.

For each of 4 class we take best by log likelihood model :

|Number of launch | log likelihood | size of class |
|----|----|----|
|24 | -211.09 | 7 from 50 |
|28 | -211.43 | 16 from 50  |
|3  | -211.60 | 5 from 50 |
|5  | -211.82 | 7 from 50 |
|remain | < -214.00 |   |

AFS was simulated with *moments*.

## Code of models

One can find all code in `code` directory.

One can run any code to get log likelihood or/and to draw picture:

`$ python model_23_dadi_code.py`

(dadi code only perform log likelihood equation, but moments code draw model as well)

## Pictures

![`model_24`](pictures/model_24.png)
![`model_28`](pictures/model_28.png)

![`model_3`](pictures/model_3.png)
![`model_5`](pictures/model_5.png)


One can find pictures in `pictures` directory.
