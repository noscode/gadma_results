#current best params = [543.5185164334689, 0.7521463766373916, 490.4912030860732, 56.13371757964396, 35.438431114535966]
import matplotlib
matplotlib.use("Agg")
import moments
import numpy as np
def generated_model(params, ns):
	Ns = params[:2]
	Ts = params[2:3]
	theta1 = 1
	sts = moments.LinearSystem_1D.steady_state_1D(sum(ns), theta=theta1, N=1.0)
	fs = moments.Spectrum(sts)

	before = [1.0]
	fs = moments.Manips.split_1D_to_2D(fs, ns[0], sum(ns[1:]))

	T = Ts[0]
	after = Ns[0:2]
	growth_funcs = [lambda t: after[0], lambda t: after[1]]
	list_growth_funcs = lambda t: [ f(t) for f in growth_funcs]
        fs.integrate(Npop=list_growth_funcs, tf=T, dt_fac=0.1, theta=theta1)

	before = after
	return fs

data = moments.Spectrum.from_file('../../data/result_syn.fs')

popt = [0.9024369699576065, 0.103278390491624, 0.06520188373172732]
ns = [12, 12]

model = generated_model(popt, ns)
ll_model = moments.Inference.ll_multinom(model, data)

print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))

try:
    import gadma
    all_boot = gadma.Inference.load_bootstrap_data_from_dir('../../data/bootstraps/syn')
    claic_score = gadma.Inference.get_claic_score(generated_model, all_boot, popt, data, pts=None, eps=1e-15)
    print('Model Composite Likelihood AIC score (CLAIC(p0)): {0}'.format(claic_score))
except ImportError:
    print('Install GADMA to calculate CLAIC score')

print('Drawing model to model_3.png')
model = moments.ModelPlot.generate_model(generated_model, popt, ns)
moments.ModelPlot.plot_model(model, 
	save_file='model_3.png',
	fig_title='Model 3 for Euphydryas gillettii, logLL: %.2f' % ll_model,
	pop_labels=['WY', 'CO'],
	nref=None,
	draw_scale=False,
	gen_time=1.0,
	gen_time_units="Genetic units",
	reverse_timeline=True)
