#current best params = [538.4252699578009, 0.7945655932708807, 516.6480620141139, 34.28359061359648, 40.44495933250036]
import dadi
def generated_model(params, ns, pts):
	Ns = params[:4]
	Ts = params[4:5]
	Ms = params[5:]
	theta1 = 1
	xx = dadi.Numerics.default_grid(pts)
	phi = dadi.PhiManip.phi_1D(xx, theta0=theta1, nu=Ns[0])
	before = [Ns[0]]
	phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
	before.append((1 - Ns[1]) * before[-1])
	before[-2] *= Ns[1]
	T = Ts[0]
	after = Ns[2:4]
	growth_func_1 = after[0]
	growth_func_2 = lambda t: before[1] + (after[1] - before[1]) * (t / T)
	phi = dadi.Integration.two_pops(phi, xx,  T=T, nu1=growth_func_1, nu2=growth_func_2, m12=0, m21=0, theta0=theta1)
	before = after
	sfs = dadi.Spectrum.from_phi(phi, ns, [xx]*2)
	return sfs

data = dadi.Spectrum.from_file('../../data/result_syn.fs')

popt = [538.4252699578009, 0.7945655932708807, 516.6480620141139, 34.28359061359648, 40.44495933250036]
pts = [20, 30, 40]
ns = [12, 12]

func_ex = dadi.Numerics.make_extrap_log_func(generated_model)
model =  func_ex(popt, ns, pts)
ll_model = dadi.Inference.ll(model, data)
print('Model log likelihood (LL(model, data)): {0}'.format(ll_model))
